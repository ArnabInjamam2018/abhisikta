<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends Frontend_Controller {

  function __construct(){
		parent::__construct();
		$this->load->model('Services_Model');
	}  
    
    
    
	public function index()
	{      
     
        $content['Services']=$this->Services_Model->GetServices();
		$content['subview']="services";
		$this->load->view('layout/default', $content);
	}
    
    public function detail($ser_id)
	{
        $content['IndividualService']=$this->Services_Model->GetIndividualService($ser_id);
		$content['subview']="services_detail";
		$this->load->view('layout/default', $content);
	}
	
    	
	
}
