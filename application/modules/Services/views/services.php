<section class="inner_banner">
    <div class="banner-area bg-overlay" id="banner-area" style="background-image:url('<?php echo base_url();?>assets/images/f1.jpg');">
       <div class="banner-heading">
           <div class="breadcrumb">
                <div class="section-headline white-headline text-center">
                    <h3>Services</h3>
                </div>
                <ul>
                    <li class="home-bread">Home</li>
                    <li>Our Services</li>
                </ul>
            </div>
       </div>
    </div>
</section>


   <section id="main-container" class="main-container ts-srevice-inner srvc_list services-area area-padding">
		<div class="container">

         <div class="row">
            <div class="col-md-12">
               <div class="section-headline text-center">
                <h3>Our Service</h3>
            </div>
            </div>
         </div>
            <div class="all-services">
<?php   // printarray($Products);
         $total_count    =   count($Services);
         $looprow_count  =   ceil($total_count/3);
         
            $m=0;
            $k=0;
           
            for($i=0;$i<$looprow_count;$i++)
            {
                ?>
            
            <div class="row">
             
             
              <?php
                
                 for($j=$k;$j<$total_count;$j++)
            {
                
              ?>
            <div class="col-lg-4 col-md-12" data-aos="fade-up">
               <div class="single-service">
                  <span class="service-img">
                     <a href="<?php echo base_url('Services/detail/'.$Services[$j]->ser_id); ?>">
                        <img class="img-fluid" src="<?php echo base_url('admin/webroot/uploads/product/'.$Services[$j]->pro_image); ?>" alt="">
                      </a>
                  </span> 
                  <div class="service-content">
                     <div class="service-icon">
                        <i class="icon-engine"></i>
                     </div> 
                     <h4><a href="<?php echo base_url('Services/detail/'.$Services[$j]->ser_id); ?>"><?php echo $Services[$j]->product_name; ?></a></h4>
                     <p><?php echo substr($Services[$j]->meta_description,0,100); ?></p>
                  </div> 
               </div> 
            </div> 
           
            <?php
                   if($m==2)
                   {
                       break;
                   }
                     $m++;
            }
            
            ?> 
             
         </div>
            
           <?php if($m==2){ 
             $m=0;
             $k=$j+1;
            ?>
            <div class="gap-30"></div>
            <?php
            } 
            
            ?>
            
         
            
            
            
            
<?php
              
            
            }
            
?>
       
            </div>
            
            
            
        

		</div>
   </section>  
   
  

   <section id="ts-testimonial-standard" class="ts-testimonial-standard pd-top0 pb-0 mt-minus95">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="testimonial-standard owl-carousel">
                  <div class="testimonial-item-single with-bg">
                     <div class="quote-item">
                        <img class="img-fluid" src="images/testimonial/quote_profile.png" alt="Jonas Blue">
                        <div class="quote-item-info">
                           <h3 class="quote-author">Donald Gonzales</h3>
                           <span class="quote-subtext">Lead Painter</span>
                        </div>
                     </div> 
                     <p class="quote-text">A wonderful serenity taken possession  into entire soul  like to these sweet of  tence  this spot which</p>
                  </div>
                  <div class="testimonial-item-single with-bg">
                     <div class="quote-item">
                        <img class="img-fluid" src="images/testimonial/quote_profile.png" alt="Jonas Blue">
                        <div class="quote-item-info">
                           <h3 class="quote-author">Donald Gonzales</h3>
                           <span class="quote-subtext">Lead Painter</span>
                        </div>
                     </div> 
                     <p class="quote-text">A wonderful serenity taken possession  into entire soul  like to these sweet of  tence  this spot which</p>
                  </div>
                  <div class="testimonial-item-single with-bg">
                     <div class="quote-item">
                        <img class="img-fluid" src="images/testimonial/quote_profile.png" alt="Jonas Blue">
                        <div class="quote-item-info">
                           <h3 class="quote-author">Donald Gonzales</h3>
                           <span class="quote-subtext">Lead Painter</span>
                        </div>
                     </div> 
                     <p class="quote-text">A wonderful serenity taken possession  into entire soul  like to these sweet of  tence  this spot which</p>
                  </div>
               </div>
            </div> 
         </div> 
      </div> 
   </section>

   <section id="ts-pertner" class="ts-pertner solid-bg">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="partner-carousel owl-carousel">
                  <figure class="partner-item partner-logo">
                     <a href="#"><img class="img-fluid" src="images/clients/client-img1.png" alt=""></a>
                  </figure> 
                  <figure class="partner-item partner-logo">
                     <a href="#"><img class="img-fluid" src="images/clients/client-img2.png" alt=""></a>
                  </figure> 
                  <figure class="partner-item partner-logo">
                     <a href="#"><img class="img-fluid" src="images/clients/client-img3.png" alt=""></a>
                  </figure> 
                  <figure class="partner-item partner-logo">
                     <a href="#"><img class="img-fluid" src="images/clients/client-img4.png" alt=""></a>
                  </figure> 
                  <figure class="partner-item partner-logo">
                     <a href="#"><img class="img-fluid" src="images/clients/client-img5.png" alt=""></a>
                  </figure> 
                  <figure class="partner-item partner-logo">
                     <a href="#"><img class="img-fluid" src="images/clients/client-img1.png" alt=""></a>
                  </figure> 
               </div> 
            </div> 
         </div> 
      </div> 
   </section> 