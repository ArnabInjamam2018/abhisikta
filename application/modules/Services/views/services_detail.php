<section class="inner_banner">
    <div class="banner-area bg-overlay" id="banner-area" style="background-image:url('<?php echo base_url();?>assets/images/f1.jpg');">
       <div class="banner-heading">
           <div class="breadcrumb">
                <div class="section-headline white-headline text-center">
                    <h3>Services Details</h3>
                </div>
                <ul>
                    <li class="home-bread">Home</li>
                    <li>Services Details</li>
                </ul>
            </div>
       </div>
    </div>
</section>


   <section id="main-container" class="main-container pb-120 srvc_dtls">
      <div id="ts-service-details" class="ts-service-detials">
         <div class="container">
            <div class="row">
              <div class="col-lg-6 col-md-12">
                  <div class="ts-service-content">
                     <span class="service-img">
                        <img class="img-fluid" src="<?php echo base_url('admin/webroot/uploads/product/'.$IndividualService[0]->pro_image); ?>" alt="">
                     </span>
                    </div>
                </div>
               <div class="col-lg-6 col-md-12">
                  <div class="ts-service-content">
                      <h2 class="section-title">
                        <?php echo $IndividualService[0]->product_name; ?>
                     </h2>
                     <h3 class="column-title no-border">
                        <span>Service </span> Description
                     </h3>
                     <p>
                         <?php echo $IndividualService[0]->product_description; ?>
                        
                      
                      
                    </p>
                      
                      
                    
                      
                      <?php 
                      
                          echo $IndividualService[0]->product_code;
                      ?>
                      
                  
                     
                      
                      
                      
                      
                 
               </div> 
            </div><!-- Main row end -->

         </div><!-- Container end -->
      </div><!-- Service details end -->
      </section><!-- Main container end -->