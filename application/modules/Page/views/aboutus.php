    <section class="inner_banner">
         <div class="banner-area bg-overlay" id="banner-area" style="background-image:url('<?php echo base_url();?>assets/images/f1.jpg');">
           <div class="banner-heading">
              <div class="breadcrumb">
                    <div class="section-headline white-headline text-center">
                        <h3>About Us</h3>
                    </div>
                    <ul>
                        <li class="home-bread">Home</li>
                        <li>About Us</li>
                    </ul>
                </div>
           </div>
        </div>
    </section>    
   
       
        <?php  $content = unserialize($home_page_con[0]->banner_description); ?>
       <div class="about-area area-padding" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="about-content">
                            <h4>Our Benifits</h4>
                            <ul>
                                <li>
                                    1. The management of "M/S AVISIKTA " are committed to operate
                                    every aspect of the business to those standards that offer the
                                    highest possible quality of service to all clients. 
                                </li>
                                <li>
                                    2. This is supported by a progressive management style that encourages
                                    the Quality culture throughout the Company.
                                </li>                                
                                <li>
                                    3. The management are committed to the continuous improvement
                                    of the Quality Management System by establishing and reviewing
                                    quality objectives for all areas of the company. 
                                </li>
                                <li>
                                    4. This is to ensure that the company operates effectively and
                                    efficiently and meets the needs of customers. 

                                </li>
                                <li>
                                   5. The effectiveness of the Quality Management System is monitored
                                    by planned audits, management reviews and effective corrective
                                    and preventive action. 

                                </li>
                                 <li>
                                    6. All personnel have been made aware of the management commitment
                                     to this policy in particular and quality in general and are encouraged to
                                     demonstrate their own support to the system by continuous active
                                     participation. 

                                </li>

                            </ul>
                      
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="about-image">
                            <img src="<?php echo base_url();?>assets/images/benifits.png" alt="">
                        </div>
                    </div>
                 
                    
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="about-image">
                            <img src="<?php echo base_url();?>assets/images/about.jpg" alt="">
                        </div>
                    </div>
                 
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="about-content">
                            <h4>Quality Assurance</h4>
                            <ul>
                                <li>
                                    1. The management of "M/S AVISIKTA " are committed to operate
                                    every aspect of the business to those standards that offer the
                                    highest possible quality of service to all clients. 
                                </li>
                                <li>
                                    2. This is supported by a progressive management style that encourages
                                    the Quality culture throughout the Company.
                                </li>                                
                                <li>
                                    3. The management are committed to the continuous improvement
                                    of the Quality Management System by establishing and reviewing
                                    quality objectives for all areas of the company. 
                                </li>
                                <li>
                                    4. This is to ensure that the company operates effectively and
                                    efficiently and meets the needs of customers. 

                                </li>
                                <li>
                                   5. The effectiveness of the Quality Management System is monitored
                                    by planned audits, management reviews and effective corrective
                                    and preventive action. 

                                </li>
                                 <li>
                                    6. All personnel have been made aware of the management commitment
                                     to this policy in particular and quality in general and are encouraged to
                                     demonstrate their own support to the system by continuous active
                                     participation. 

                                </li>

                            </ul>
                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
      <section class="mission_vision">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6" data-aos="fade-up">
                        <div class="image_sec">
                        <img src="<?php echo base_url('admin/webroot/uploads/home/').$content[1][1]; ?>">
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-up">
                        <div class="section-headline">
						    <h3>Our Mission</h3>
						</div>
                        <div class="content">
                            <!-- <p>To be a company committed to "ON TIME & ON BUDGET" delivery, guaranteed performance and offering end-to-end solutions to our Clients</p>
                            <p>To achieve this, we are focused to provide a level of service that will exceed the expectations of our clients, maintain the highest level of quality and provide our employees an opportunity to grow and prosper.</p>
                            <ul>
                                <li>1. Aim to deliver best value at all times whilst continuing to develop our people skills through advance training opportunities.  </li>
                                <li>2. Make continuous improvements to our services and people.  </li>
                                <li>3. Operate an open and honest relationship with all of our clients and this includes joint decisions making processes, agreed objectives and anticipated goals. </li>
                                
                            </ul> -->
                            <?php echo $content[1][0];?>
                        </div>
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-6">
                        <div class="section-headline">
						    <h3>Our Vision</h3>
						</div>
                        <div class="content" data-aos="fade-up">
                            <!-- <p>"To be recognized as a leading contracting company in India, through our services as per customer satisfaction</p>
                            <p>In order to achieve this vision, our goals are: </p>
                            <ul>
                                <li>1. To continue to provide and develop strong leadership with integrity</li>
                                <li>2. To constantly strive for continuity of works, taking pride in our aim of delivering all projects safely, on budget and ahead of program, ultimately exceeding client expectation.  </li>
                                <li>3. To respect the well-being of our workers and protect the environment, maintaining our "zero harm" record. </li>
                                <li>4.To promote continual improvement of our Integrated Management System, adopting innovations and efficiencies without compromising environmental or safety standards.  </li>
                                <li>5.To recognise and encourage our employees, providing skills development opportunities to help them develop in their careers.  </li>
                               
                            </ul> -->
                            <?php echo $content[2][0];?>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-up">
                        <div class="image_sec">
                        <img src="<?php echo base_url('admin/webroot/uploads/home/').$content[2][1]; ?>">
                        </div>
                    </div>
                   
                </div>
            </div>
        </section>
       