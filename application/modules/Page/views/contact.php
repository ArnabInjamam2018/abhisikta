
<?php

  

?>


<section class="inner_banner">
    <div class="banner-area bg-overlay" id="banner-area" style="background-image:url('<?php echo base_url();?>assets/images/f1.jpg');">
       <div class="banner-heading">
           <div class="breadcrumb">
                <div class="section-headline white-headline text-center">
                    <h3>Contact Us</h3>
                </div>
                <ul>
                    <li class="home-bread">Home</li>
                    <li>Contact Us</li>
                </ul>
            </div>
       </div>
    </div>
</section>
        
       
        <div class="contact-page area-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12" data-aos="fade-up">
                        <div class="contact-head">
                            <h3>Contact us</h3>
                            <p><?php echo  $this->website['data']->company_name; ?></p>
<!--                            <p><b>Reg Office / Head Office :</b></p>-->
                            <p><?php echo  $this->website['data']->address; ?></p>
                            <p><b>Work :</b> </p>
                            <p><?php echo  $this->website['data']->corporate_address; ?></p>
                            <P><b>Ph. No. :</b> <?php echo  $this->website['data']->support_contact; ?></P>
                            <P><b>Email :</b>  <?php echo  $this->website['data']->support_email; ?></P>
                             <P><b>website :</b><?php echo  $this->website['data']->site_url; ?> </P>                                             
                            

                            <div class="project-share">
                                <h5>Our Social Link :</h5>
                                <ul class="project-social">
                                     <li>
                                        <a href="<?php echo $this->website['data']->facebook_link;?>">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $this->website['data']->twitter_link; ?>">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $this->website['data']->google_plus_link; ?>">
                                            <i class="fa fa-google"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                



                  
                    <div class="col-md-6 col-sm-6 col-xs-12" data-aos="fade-up">
                        <div class="contact-form">
                            <div class="row">
                                <form id="contactMeEstimatedsgdsgsdgsdhdshdshshreh" method="POST" class="contact-form">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="name_two" class="form-control" placeholder="Name" required data-error="Please enter your name">
                                         <span id="show_error_name" style="color:red;display:none;">The name field is Required</span>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" class="email form-control" id="email_two" placeholder="Email" required data-error="Please enter your email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" id="phone_number_two" class="form-control" placeholder="Phone Number" required data-error="Please enter your message subject">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <textarea id="message_new" rows="7" placeholder="Message" class="form-control" required data-error="Write your message"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                        <button type="button" id="sendenquirytwo" class="contact-btn">Submit</button>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                        <p id="success_msg_dtata" class="h3 text-center" style="color:green;display:none;">Thank your For your message.We will reach you soon.</p> 
                                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                                        <div class="clearfix"></div>
                                    </div>   
                                </form>  
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
       




<br>
<br>
<br>




        <div class="map-area-top">
            <div class="container-fluid">
                
                <div class="row">
                    
                   
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        
                        <div style="width: 100%">
                            
                          <?php echo $this->website['data']->address_iframe;?>
                        
                        </div>
                        <br />
                  
                   
                     
                    </div>
                </div>
            </div>
        </div>
 
