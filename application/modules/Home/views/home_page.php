     <section>
        <div class="intro-area">
            <div class="main-overly"></div>
            <div class="intro-carousel">
                <?php
                    //print_r($banner_details);
                    for($i=0;$i<count($banner_details);$i++)
                    {
                    ?>
                <div class="intro-content">
                    <div class="slider-images">
                        <img src="<?php echo base_url('admin/webroot/uploads/banner/').$banner_details[$i]->image_name; ?>" alt="">
                       
                    </div>
                    <div class="slider-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="layer-1">
                                                 <h1><?php echo $banner_details[$i]->image_seo_title; ?></h1>
                                            </div>
                                            <div class="layer-2 ">
                                                <p><?php echo $banner_details[$i]->image_url_link; ?></p>
                                            </div>
                                            <div class="layer-3">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } 
                
                
                $content = unserialize($home_page_con[0]->banner_description);
               
                
                ?>

            </div>
        </div>
        </section>
        <section class="why_us">
            <div class="container">
                <div class="row">
                    <div class="col-md-6" data-aos="fade-up">
                        <div class="image_sec" > 
                            <img src="<?php echo base_url('admin/webroot/uploads/home/').$content[0][1]; ?>">
                        </div>
                    </div>
                    <div class="col-md-6 " data-aos="fade-up">
                        <div class="section-headline">
						    <h3>Why Us</h3>
						</div>
                        <div class="content">
                        <?php echo $content[0][0]; ?>
                            <!-- <p>We have attained a remarkable position in the market by offering supreme quality products and services to our clients. Owing to our transparent monetary transactions, fair business dealings and ethical business policies, we have garnered a huge clientele across the country. Listed below are some of the other factors behind our success:</p>
                            <ul>
                                <li>1. Timely and versatile execution capability </li>
                                <li>2. Team of qualified and experienced professionals </li>
                                <li>3. Quality products and services </li>
                                <li>4.0Priorities such as economy, efficiency, reliability, as well as quick response  </li>
                                <li>5.0Focused business approach  </li>
                                <li>6. Wide distribution network  </li>
                                <li>7. Competitive prices   </li>
                            </ul> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="service_sec" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-headline text-center">
						    <h3>Our Service</h3>
						</div>
					</div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="service">
                             <img src="<?php echo base_url('assets/img/s1.png'); ?>">
                            <p>CONSULTANCY</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="service">
                             <img src="<?php echo base_url('assets/img/s2.png'); ?>">
                            <p>ENGINEERS & CONTRACTORS</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="service">
                             <img src="<?php echo base_url('assets/img/s3.png'); ?>">
                            <p>MAINTENANCE</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="service">
                             <img src="<?php echo base_url('assets/img/s4.png'); ?>">
                            <p>SUPPLY</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section>
            <div class="services-area services-4 area-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-headline text-center">
						    <h3>Our Projects Locations</h3>
						</div>
					</div>
				</div>
                <div class="row text-center">
                    <div class="all-services">
                        
                          <?php 
                        
                    //count($Product)
                        
                        if($Product)
                        {
                            
                     
                            for($j=0;$j<6;$j++) 
                            
                                {
                                   



                                ?>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="single-service" data-aos="fade-up">
                                <a class="service-image" href="<?php echo base_url('Products/categories/'.$Product[$j]->cat_id); ?>">
                                    
                                   
                                    
                                    <img src="<?php echo base_url('admin/webroot/uploads/cat/'.$Product[$j]->cat_image); ?>" alt="">
                                </a>
                                <div class="service-content">
                                    <a class="service-image" href="<?php echo base_url('Products/categories/'.$Product[$j]->cat_id); ?>"><h4><?php echo $Product[$j]->cat_name; ?></h4></a>

<!--                                    <a class="service-btn" href="<?php echo base_url('Products/categories/'.$Product[$j]->cat_id); ?>">Go To Products</a>-->
                                </div>
                            </div>
                        </div>
                       
                         <?php
                    
                          } 
                     }

                         ?>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <a href="<?php echo base_url('Products'); ?>">
                                <button type="button"  class="quote-btn more">See More</button></a>
                        </div>
                    </div>
				</div>
			</div>
		</div>
        </section>


        <section class="mission_vision">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6" data-aos="fade-up">
                        <div class="image_sec">
                            <img src="<?php echo base_url('admin/webroot/uploads/home/').$content[1][1]; ?>">
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-up">
                        <div class="section-headline">
						    <h3>Our Mission</h3>
						</div>
                        <div class="content">
                            <!-- <p>To be a company committed to "ON TIME & ON BUDGET" delivery, guaranteed performance and offering end-to-end solutions to our Clients</p>
                            <p>To achieve this, we are focused to provide a level of service that will exceed the expectations of our clients, maintain the highest level of quality and provide our employees an opportunity to grow and prosper.</p>
                            <ul>
                                <li>1. Aim to deliver best value at all times whilst continuing to develop our people skills through advance training opportunities.  </li>
                                <li>2. Make continuous improvements to our services and people.  </li>
                                <li>3. Operate an open and honest relationship with all of our clients and this includes joint decisions making processes, agreed objectives and anticipated goals. </li>
                                
                            </ul> -->
                            <?php echo $content[1][0];?>
                        </div>
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-6" data-aos="fade-up">
                        <div class="section-headline">
						    <h3>Our Vision</h3>
						</div>
                        <div class="content" >
                            <!-- <p>"To be recognized as a leading contracting company in India, through our services as per customer satisfaction</p>
                            <p>In order to achieve this vision, our goals are: </p>
                            <ul>
                                <li>1. To continue to provide and develop strong leadership with integrity</li>
                                <li>2. To constantly strive for continuity of works, taking pride in our aim of delivering all projects safely, on budget and ahead of program, ultimately exceeding client expectation.  </li>
                                <li>3. To respect the well-being of our workers and protect the environment, maintaining our "zero harm" record. </li>
                                <li>4.To promote continual improvement of our Integrated Management System, adopting innovations and efficiencies without compromising environmental or safety standards.  </li>
                                <li>5.To recognise and encourage our employees, providing skills development opportunities to help them develop in their careers.  </li>
                               
                            </ul> -->
                            <?php echo $content[2][0];?>
                        </div>
                    </div>
                    <div class="col-md-6" data-aos="fade-up">
                        <div class="image_sec">
                            <img src="<?php echo base_url('admin/webroot/uploads/home/').$content[2][1]; ?>">
                        </div>
                    </div>
                   
                </div>
            </div>
        </section>


        <section class="testimonial_sec">
            <div class="container">
                <div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-headline text-center">
						    <h3>What People Say</h3>
						</div>
					</div>
				</div>
	           <div class="row" data-aos="fade-up">
		<div class="col-sm-12">
        <div class="seprator"></div>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Wrapper for slides -->
              <div class="carousel-inner">
              <?php                         
//                  echo "<pre>";
//                  print_r($tes_details);
                         for($i=0;$i<count($tes_details)-1;$i++)
                         {
                             $j=$i+1
              ?>

                <div class="item <?php if($i==0) { ?> active <?php } ?>">
                  <div class="row" style="padding: 20px">
                      <div class="col-md-6">
                          <div class="testi_wrap">
                         <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                            <p class="testimonial_para"><?php echo $tes_details[$i]->tes_description; ?></p><br>
                            <div class="row">
                            <div class="col-sm-2">
                                <img src="<?php echo base_url('admin/webroot/uploads/testimonial/').$tes_details[$i]->image_name; ?>" class="img-responsive" style="width: 80px">
                                </div>
                                <div class="col-sm-10">
                                <h4><?php echo $tes_details[$i]->client_name; ?></h4>
                                <p class="testimonial_subtitle"><span><?php echo $tes_details[$i]->test_title; ?></span><br>
                                <!-- <span>Officeal All Star Cafe</span> -->
                                </p>
                            </div>
                            </div>
                      </div>
                      </div>
                      
                       <div class="col-md-6">
                          <div class="testi_wrap">
                         <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                            <p class="testimonial_para"><?php  echo $tes_details[$j]->tes_description; ?></p><br>
                            <div class="row">
                            <div class="col-sm-2">
                                <img src="<?php echo base_url('admin/webroot/uploads/testimonial/').$tes_details[$j]->image_name; ?>" class="img-responsive" style="width: 80px">
                                </div>
                                <div class="col-sm-10">
                                <h4><?php echo $tes_details[$j]->client_name; ?></h4>
                                <p class="testimonial_subtitle"><span><?php echo $tes_details[$j]->test_title; ?></span><br>
                                <!-- <span>Officeal All Star Cafe</span> -->
                                </p>
                            </div>
                            </div>
                      </div>
                      </div>
                      <!-- <div class="col-md-6">
                          <div class="testi_wrap">
                         <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                            <p class="testimonial_para">Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo en.</p><br>
                            <div class="row">
                            <div class="col-sm-2">
                                <img src="http://demos1.showcasedemos.in/jntuicem2017/html/v1/assets/images/jack.jpg" class="img-responsive" style="width: 80px">
                                </div>
                                <div class="col-sm-10">
                                <h4><strong>Jack Andreson</strong></h4>
                                <p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                <span>Officeal All Star Cafe</span>
                                </p>
                            </div>
                            </div>
                      </div>
                      </div> -->
                   
                  </div>
                </div>

             <?php }  $j = 0; ?>
               <!-- <div class="item">
                   <div class="row" style="padding: 20px">
                       <div class="col-md-6">
                           <div class="testi_wrap">
                            <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                            <p class="testimonial_para">Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo en.</p><br>
                            <div class="row">
                            <div class="col-sm-2">
                                <img src="http://demos1.showcasedemos.in/jntuicem2017/html/v1/assets/images/kiara.jpg" class="img-responsive" style="width: 80px">
                                </div>
                                <div class="col-sm-10">
                                <h4><strong>Kiara Andreson</strong></h4>
                                <p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                <span>Officeal All Star Cafe</span>
                                </p>
                            </div>
                            </div>
                       </div>
                       </div> 
                        <div class="col-md-6">
                            <div class="testi_wrap">
                                <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                                <p class="testimonial_para">Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo en.</p><br>
                                <div class="row">
                                <div class="col-sm-2">
                                    <img src="http://demos1.showcasedemos.in/jntuicem2017/html/v1/assets/images/kiara.jpg" class="img-responsive" style="width: 80px">
                                    </div>
                                    <div class="col-sm-10">
                                    <h4><strong>Kiara Andreson</strong></h4>
                                    <p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                    <span>Officeal All Star Cafe</span>
                                    </p>
                                </div>
                                </div>
                            </div>
                       </div>
                  </div>
                </div> -->


              </div>
            </div>
            <div class="controls testimonial_control">
                <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                  data-slide="prev"></a>

                <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                  data-slide="next"></a>
              </div>
        </div>
        
	</div>
            </div>

        </section>

<!--
         <div class="brand-area area-padding">
            <div class="test-overly"></div>
			<div class="container">
                <div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-headline text-center">
						    <h3>We Deal In</h3>
						</div>
					</div>
				</div>
				<div class="row" data-aos="fade-up">
                    <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="Reviews-content">
							
							<div class="brand-carousel item-indicator">
                                
                                
                            <?php for($i=0;$i<count($homepartner_page_con);$i++) {   
                                if(count($homepartner_page_con)>0){
                                     foreach($homepartner_page_con as $homepartner){
                                         
                                   
                                   
                                ?>  
                       
                                <div class="single-testi text-center">
                                    <div class="testi-img ">
                                        <img src="<?php echo site_url('admin/webroot/uploads/partner/'.$homepartner->image_name);?>" alt="">
                                    </div>
                                    
                                </div>
                              
                              <?php }  
                                  
                                }
                                 } 
                                
                                ?>
                              
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
-->
      
      
     
	
	
       
       