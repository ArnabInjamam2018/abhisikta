<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Frontend_Controller {

  function __construct(){
		parent::__construct();
		$this->load->model('Home_Model');
        $this->load->library('form_validation');
        $this->load->library('email');
	}  
    
    
    
	public function index()
	{
        
        $content['Product']=$this->Home_Model->get_all_categories();
        $content['banner_details']=$this->Home_Model->GetBanner();
         $content['homepartner_page_con']=$this->Home_Model->get_all_homepartner();
		 $content['home_page_con']=$this->Home_Model->get_all_content();
		 $content['tes_details']=$this->Home_Model->GetTestimonial();
		$content['subview']="home_page";
		$this->load->view('layout/default', $content);
	}
    
    
    function send_query()
	{
       
        
		$RequestMethod=$this->input->server("REQUEST_METHOD");
		if($RequestMethod == "POST"){
			$this->form_validation->set_rules('name','Name', 'trim|required');
			$this->form_validation->set_rules('email', 'email id', 'trim|required|valid_email');
			$this->form_validation->set_rules('phone_number', 'Mobile Number', 'required|regex_match[/^[0-9]{10}$/]');
			$this->form_validation->set_rules('message',' message ', 'trim|required');
		if ($this->form_validation->run() == FALSE)
			{
				 $this->form_validation->set_error_delimiters('<span class="listinghotel_error" style="color:red">', '</span>');
				 $field=array('name','email','phone_number','message');
				 $error=[];
				 foreach($_POST as $key=>$val)
				 {
					 if(in_array($key,$field)){
					 $error[$key]=form_error($key);
					 }
				 }

				 $data=array("type"=>"error","message"=>$error);
				 echo json_encode($data); 
			}else {
				$data=array(
				  'name'=>$this->input->post('name'),
				  'mobile'=>$this->input->post('phone_number'),
				  'email'=>$this->input->post('email'),
				  'message'=>$this->input->post('message'),
				  'other'=>serialize($_POST),
				  'category'=>'General Enquiry'
				
				);
                 
            
                 $subject = "Enquiry Email";
				 $UserMail=$this->load->view('enquiry_mail_user',$data,true);
				 $AdminMail=$this->load->view('enquiry_mail_admin',$data,true);
            
            
           
                                 
                                
				$AdminEmailId=$this->website['data']->support_email;
				 email_send($_POST['email'],$subject,$UserMail);
				 email_send($AdminEmailId,$subject,$AdminMail); 
				$true=$this->Home_Model->inser_query($data);
				if($true){
				 $data = array("type"=>'success');
	                           echo json_encode($data);
				}else{
					$data=array("type"=>"error","message"=>"some error occurred. Try again.. ");
			      echo json_encode($data);
				}
			}
		}else{
			$data=array("type"=>"error","message"=>"some error occurred. Try again.. ");
			 echo json_encode($data);
			
		}		
				
	}
    
    
      function send_query_two()
	{
        
        
		$RequestMethod=$this->input->server("REQUEST_METHOD");
		if($RequestMethod == "POST"){
			
            
				$data=array(
				  'name'=>$this->input->post('name'),
				  'mobile'=>$this->input->post('phone_number'),
				  'email'=>$this->input->post('email'),
				  'message'=>$this->input->post('message'),
				  'other'=>serialize($_POST),
				  'category'=>'Contact Us'
				
				);
                 
            
                 $subject = "Contact Us Query Email";
				 $UserMail=$this->load->view('enquiry_mail_user',$data,true);
				 $AdminMail=$this->load->view('enquiry_mail_admin',$data,true);
                                 
                                
				$AdminEmailId=$this->website['data']->support_email;
				 email_send($_POST['email'],$subject,$UserMail);
				 email_send($AdminEmailId,$subject,$AdminMail); 
				$true=$this->Home_Model->inser_query($data);
				if($true){
				 $data = array("type"=>'success');
	                           echo json_encode($data);
				}else{
					$data=array("type"=>"error","message"=>"some error occurred. Try again.. ");
			      echo json_encode($data);
				}
			
            
	}
	
	
      }
    
    function category()
    {
         $content['categories']=$this->Home_Model->get_all_categories();
        
        echo json_encode($content['categories']);
    }
      
   
    
  
	
}
