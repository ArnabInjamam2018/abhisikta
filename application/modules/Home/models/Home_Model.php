<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_Model extends MY_Model{
	
	

	function GetBanner(){
	
		$this->db->select('*');
		$this->db->where('status','active');
		$query=$this->db->get('banner');
		if($query->num_rows()==' '){
			return false;
		}else{
			 return $query->result();
		}
	}
	function GetBlog(){
	
		$this->db->select('*');
		$this->db->where('status','yes');
		$query=$this->db->get('tbl_posts_blog');
		if($query->num_rows()==' '){
			return false;
		}else{
			 return $query->result();
		}
	}
	function get_all_content()
	{
  
		  $this->db->select('*');
		  $this->db->where('status','active');
		  $query=$this->db->get('homepagecontent');
		  if($query->num_rows()==' '){
			  return false;
		  }else{
			   return $query->result();
		  }      
	}
  function  GetTestimonial(){


		$this->db->select('*');
		$this->db->where('status','active');
		$query=$this->db->get('testimonial');
		if($query->num_rows()==' '){
			return false;
		}else{
			 return $query->result();
		}   
  }
    
    function  GetServices(){


	
		$this->db->from('service_master');
		$query=$this->db->get();
       
		if($query->num_rows()==''){
			  return false;
		}else{
			  return $query->result();
		}    
  }

 function  GetProduct(){


		$this->db->select('*');
		$this->db->where('status','active');
        $this->db->limit(6);
		$query=$this->db->get('product_master');
		if($query->num_rows()==' '){
			return false;
		}else{
			 return $query->result();
		}   
  }
function  BoxContent(){


		$this->db->select('*');
	
		$query=$this->db->get('academicpagecontent');
		if($query->num_rows()==' '){
			return false;
		}else{
			 return $query->result();
		}   
  }


  
    
    function inser_query($data)
	{
		$this->db->insert('query_list',$data);
		return true;
		
	}
    
     function get_all_categories()
    {
         $this->db->select('*');
		$this->db->order_by("cat_id", "desc");
		$query=$this->db->get('categories');
		if($query->num_rows()==' '){
			return false;
		}else{
			 return $query->result();
		}       
    }
       function get_all_homepartner()
    {
            $this->db->select('*');
			$this->db->where('status','active');
			$this->db->where('image_for',1);
		    $query=$this->db->get('partner');
		if($query->num_rows()==' '){
			return false;
		}else{
			 return $query->result();
		}       
    }
	
}