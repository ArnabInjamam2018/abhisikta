<?php
    $banner = $banner_details[0];
?>
   
   

<section class="inner_banner">
    <div class="banner-area bg-overlay" id="banner-area" style="background-image:url('<?php echo base_url();?>assets/images/f1.jpg');">
       <div class="banner-heading">
           <div class="breadcrumb">
                <div class="section-headline white-headline text-center">
                    <h3>Career</h3>
                </div>
                <ul>
                    <li class="home-bread">Home</li>
                    <li>Career</li>
                </ul>
            </div>
       </div>
    </div>
</section>


  


<section class="team_sec area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-headline text-center">
                     <h3>Our Team</h3>
                </div>
            </div>
        </div>
        <div class="member_wrap">
            <div class="row">


            <?php 
              //echo "<pre>";
              //print_r($carrer_page_content);
              for($i=0;$i<count($carrer_page_content);$i++)
              {
            ?>
                 
            <div class="col-md-4">
                <div class="member" >
                    <img src="<?php echo base_url('/admin/webroot/uploads/user_images/').$carrer_page_content[$i]->image; ?>">
                    <div class="details">
                        <h4 class="name"><?php echo $carrer_page_content[$i]->user_name; ?></h4>
                        <p class="post"> <?php echo $carrer_page_content[$i]->heading; ?></p>
                        <a class="service-btn"  onclick="show_message('<?php echo $carrer_page_content[$i]->user_id; ?>');">My Message</a>
                    </div>
                </div>
            </div>

            <?php
              }
            
            ?>
            <!-- data-toggle="modal" data-target="#member" -->
            <!-- <div class="col-md-4">
                <div class="member">
                    <img src="<?php echo base_url('assets/images/t2.jpg'); ?>">
                    <div class="details">
                        <h4 class="name">Member 2</h4>
                        <p class="post"> Chair Man</p>
                        <a class="service-btn" data-toggle="modal" data-target="#member" >My Message</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="member">
                    <img src="<?php echo base_url('assets/images/t3.jpg'); ?>">
                    <div class="details">
                        <h4 class="name">Member 3</h4>
                        <p class="post"> Project Manager</p>
                        <a class="service-btn" data-toggle="modal" data-target="#member" >My Message</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="member">
                    <img src="<?php echo base_url('assets/images/t4.jpg'); ?>">
                    <div class="details">
                        <h4 class="name">Member 4</h4>
                        <p class="post"> Project Manager</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="member">
                    <img src="<?php echo base_url('assets/images/t5.jpg'); ?>">
                    <div class="details">
                        <h4 class="name">Member 5</h4>
                        <p class="post"> HOD</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="member">
                    <img src="<?php echo base_url('assets/images/t6.jpg'); ?>">
                    <div class="details">
                        <h4 class="name">Member 6</h4>
                        <p class="post"> HR</p>
                    </div>
                </div>
            </div> -->

        </div>
        </div>
        
    </div>
</section>


<section class="career_form_sec">
     <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-headline text-center">
                     <h3>Career</h3>
                </div>
                <!-- <?php //echo base_url();?>Career/career_submit -->
                <form method="post" action="#" enctype="multipart/form-data">
                <div class="profile_from">
                  <div class="row">
                        <div class="col-md-6">
                             <label>Name</label>
                            <input class="form-control" type="text" id="first_name_join" name="doctor_name" >
                            <p id="show_error_name" style="display:none;color:red">Required</p>
                        </div>
                        <div class="col-md-6">
                             <label>Email</label>
                            <input class="form-control" type="text" id="email_join" >
                            <p id="show_error_email" style="display:none;color:red">Required</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <label>Phone Number</label>
                            <input class="form-control" type="text" id="phone_num_join" >
                            <p id="show_error_phone_number" style="display:none;color:red">Required</p>
                        </div>
                        <div class="col-md-6">
                             <label>Upload CV</label>
                            <input class="form-control" type="file" id="cv_join_us" >
                            <p id="show_error_message_cv" style="display:none;color:red">Required</p>
                        </div>
                    </div>
                      <div class="row">
                          <div class="col-md-12">
                              <label>Message</label>
                              <textarea class="form-control" id="message_join_us" placeholder="Write your Message"></textarea>
                              <p id="show_error_message" style="display:none;color:red">Required</p>
                          </div>
                      </div>
                    </div>  
                    <div class="sbmt">
                        <p class="success_msg_dtata_career" style="display:none;color:green">Thanks for the Information.We will get back to You.</p>
                        <button type="button" class="contact-btn" onclick="career_join_us()">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</section>

<div class="modal fade member_modal" id="member" tabindex="-1" role="dialog" aria-labelledby="member" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
<!--        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>-->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6" id="image_div">
                 <img src="<?php echo base_url('assets/images/t1.jpg'); ?>">
            </div>
            <div class="col-md-6">
                 <h4 class="name" id="heading_name">Member 1</h4>
                <select class="form-control" id="english_bengali" onchange=bring_me_lang();>
                    <option value="1">Bengali</option>
                    <option value="2">English</option>
                </select>
                <br>
                <p id="content_of_team">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="home_url" value="<?php echo base_url(); ?>">
<script>
    function show_message(user_id)
    {
        //alert(user_id);
       
        var home_url = $("#home_url").val();
        $.ajax({
			url: home_url+'Career/get_user_info',
			type: "post",
			dataType: "json",
		    data : {
                user_id:user_id
             }, 
			success: function(res){

                //var res = JSON.parse(data);
         
              console.log(res[0].user_id);
                
                if(res[0].user_id == 15)
                    {
                        $("#english_bengali").show();
                    }
                else
                {
                    $("#english_bengali").hide();
                }
             $("#image_div").html('<img src='+home_url+'/admin/webroot/uploads/user_images/'+res[0].image+'>');
             $("#heading_name").html(res[0].user_name);
             $("#content_of_team").html(res[0].description);
             $("#member").modal("show");			
				
				
			}
		});
    }
    
    function bring_me_lang()
    {
         var d = $("#english_bengali").val();
        
        if(d == 1)
            {
                  $("#content_of_team").html(`প্রিয় বন্ধুরা, <br><br>

                 অন্তরের আত্মা থেকে প্রত্যেক কর্মীবৃন্দ, শ্রমিক, মিস্ত্রি, প্রত্যক্ষ ও পরোক্ষ কর্মীবৃন্দ এবং তাদের পরিবার, প্রত্যেককে জানায় আমার হার্দিক শ্রদ্ধা ও আন্তরিক ভালোবাসা। আপনাদের ঐকান্তিক প্রচেষ্টা ছাড়া 'অভিষিক্তা' - এত  বড়ো হতে পারতো না। তাই আমাদের সম্মিলিত প্রচেষ্টাকে আমি কুর্নিশ জানায়। আমাদের দলগত প্রচেষ্টা এই কোম্পানিকে অনেকদূর এগিয়ে নিয়ে যাবে এই আশা রাখি। আমাদের প্রত্যেকের নিষ্ঠা, সততা, স্বচ্ছতা, বন্ধুত্বপূর্ণ মানসিকতা ও স্বচ্ছ ভাবমূর্তি, আজ 'অভিষিক্তা' - কে  জেলা থেকে পশ্চিমবঙ্গের কোণায় কোণায় সুনামের সঙ্গে স্থাপন করেছে। আমি গর্ব বোধ করি, ছোট্ট একটা প্রতিনিধিত্ব থেকে ব্যবসা জগতে শিল্পপতি হওয়ার যে স্বপ্ন দেখেছিলাম সেই স্বপ্নের বাস্তব রূপায়িত করতে পেরে। প্রত্যেকের কাছে আমি চির কৃতজ্ঞ এবং কুর্নিশ জানায়। আমি আশা রাখি আমাদের এই সম্মিলিত প্রচেষ্টা ও একাগ্রতা মানসিকতাকে পাথেয় করে একদিন আমরা সাফল্যের শিখরে পৌঁছে যাব। সবাই ভালো থেকো, সুস্থ থেকো।<br><br>

                                                            - সুদীপ্ত`);
            }
        else if(d == 2)
                {
                  $("#content_of_team").html(`Dear friends, <br><br>

   From the bottom of my heart, I would like to express my heartfelt respect and sincere love to every worker, employee, mechanic direct and indirect and their family. Without your diligent efforts, AVISIKTA  could not have been so big. Kudos to our joint efforts. We hope that our team efforts will take this company a long way. The dedication, honesty, transparency, friendly mentality and transparent image of each of us, today has established AVISIKTA with a reputation from the district to the corners of West Bengal. I am proud to be able to make the dream of becoming an entrepreneur in the business world come true from a small representation. I am forever grateful to everyone and express my gratitude. I hope that one day we will reach the pinnacle of success with this concerted effort and concentration mentality. All be well, be healthy.<br><br>

                                                            - Sudipta`);
                }
    }
</script>


