<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends Frontend_Controller {

  function __construct(){
		parent::__construct();
        $this->table_name="query_list";
		$this->load->model('Career_Model');
	}  
    
    
    
	public function index()
	{
       // $content['banner_details']=$this->Career_Model->GetBanner();
       // $content['carrer_details']=$this->Career_Model->GetCarrerContent();
        $content['carrer_page_content']=$this->Career_Model->GetCarrerContent();
		$content['subview']="career";
//        echo "<pre>";print_r($content);die;
		$this->load->view('layout/default', $content);
	}


    public function get_user_info()
    {
        $user_id = $this->input->post('user_id');
        $data_single_user = $this->Career_Model->GetUserInfo($user_id);
        echo json_encode($data_single_user);
    }
    
 	public function career_submit(){
        $type = $this->input->post('type');
        $name = $this->input->post('first_name_join');
        $email = $this->input->post('email_join');
        $mobile = $this->input->post('phone_num_join');
        $message = $this->input->post('message_join_us');
        
        $picture_name_2="";
        $picture2="";    
        if(!empty($_FILES['fileToUpload']['name'])){
            $config['upload_path'] = FCPATH .'assets/images/resume';
            $config['allowed_types'] =  'jpg|jpeg|png|gif|pdf|docx';
             $config['max_size']    = '15000000';
            $config['file_name'] = time().$_FILES['fileToUpload']['name'];
            //Load upload library and initialize configuration
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            
            if($this->upload->do_upload('fileToUpload')){
                $uploadData = $this->upload->data();
                 $picture_name_2 = $uploadData['file_name'];
            }else{
                 $picture_name_2 = "Problem";
            }
        }else{
             $picture_name_2 = $this->input->post('earlier_file');;
        }
        
        
        
        // echo ($picture_name_2);
        
        // die();
        
        $data=array(    
                'category'=>"Careers",
                'name'=>$name,
                'email'=>$email,
                'mobile'=>$mobile,
                'message'=>$message,
                'other'=>$picture_name_2
             );
//        echo json_encode($data);die;
          $this->Career_Model->insert_data($this->table_name,$data);
          //$this->session->set_flashdata('alert', array('message' => 'Data successfully Saved','class' => 'success'));
        $cvsurl = $this->config->item('base_url').'assets/images/resume/'.$picture_name_2;
        
         $subject =  "Application For Job At Avisikta Electricals";
         $msg_for_user = '<html> 
    
    <head> 
        <title>Recently Someone Applied From Career  Form</title> 
    </head> 
    <body> 
      <div style="width:100%; height: 100vh; display: flex; justify-content: center; align-items: center;">
            <div>
        <h1>Application For Job At Avisikta Electricals!</h1> 
        <table cellspacing="0" style="border: 2px dashed #FB4314; padding: 14px; width: 100%; text-align: left;"> 
            <tr> 
                <th style="padding: 10px 0px; width:auto;">Name:</th><td  style="padding: 10px 0px;text-align: right;">'.$name.'</td> 
            </tr> 
            <tr> 
                <th style="padding: 10px 0px; width:auto;">Email:</th><td style="padding: 10px 0px;text-align: right;">'.$email.'</td> 
            </tr> 
            <tr> 
                <th style="padding: 10px 0px;width:auto;">Mobile Number:</th><td style="padding: 10px 0px;text-align: right;">'.$mobile.'</td> 
            </tr>
            <tr> 
                <th style="padding: 10px 0px;width:auto;">Message:</th><td style="padding: 10px 0px;text-align: right;">'.$message.'</td> 
            </tr>
            <tr> 
                <th style="padding: 10px 0px;width:auto;">Appliers Cv:</th><td style="padding: 10px 0px;text-align: right;"><a href='.$cvsurl.'>Click Here</a></td> 
            </tr> 
        </table> 
        </div>
        </div>
    </body> 
    </html>';
        
        
        
         email_send($email,$subject,$msg_for_user);
        
        
          if($data)
          {
              $res = array("stat"=>200,"Message"=>"You have successfully applied.");
          }

       // echo $msg_for_user;
        
        echo json_encode($res);
        
    }
}
