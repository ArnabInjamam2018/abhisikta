<?php
    $banner = $banner_details[0];
?>


<section class="inner_banner">
    <div class="banner-area bg-overlay" id="banner-area" style="background-image:url('<?php echo base_url();?>assets/images/f1.jpg');">
       <div class="banner-heading">
           <div class="breadcrumb">
                <div class="section-headline white-headline text-center">
                    <h3>Client</h3>
                </div>
                <ul>
                    <li class="home-bread">Home</li>
                    <li>Client</li>
                </ul>
            </div>
       </div>
    </div>
</section>


        <div class="brand-area area-padding">
            <div class="test-overly"></div>
			<div class="container">
                <div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-headline text-center">
						    <h3>Our Client</h3>
						</div>
					</div>
				</div>
				<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="Reviews-content">
							<div class="item-indicator">
                                <div class="row">
                               
                                        <?php   
                                            if(count($partner_page_con)>0){
                                                 foreach($partner_page_con as $partner){



                                            ?>  
                                         
                                            <div class="col-md-2" data-aos="fade-up">
                                            <div class="single-testi text-center">
                                                <div class="testi-img ">
                                                    <img src="<?php echo site_url('admin/webroot/uploads/partner/'.$partner->image_name);?>" alt="">
                                                </div>

                                            </div>
                                         </div>
                                       

                                          <?php }  

                                            }
                                             

                                            ?>
                                   
                                
                                 </div>
                             
                              
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>



