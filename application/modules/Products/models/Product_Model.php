<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_Model extends MY_Model{
	
	

 function  GetProduct(){


       $this->db->from('product_master');
   
		$query=$this->db->get();
		if($query->num_rows()==''){
			  return false;
		}else{
			  return $query->result();
		}   
  
 
 }
    function  GetProductCat($cat_id){


		$this->db->from('product_master');
       $this->db->where('cat_id',$cat_id);
       $this->db->where('status','active');
		$query=$this->db->get();

        
		if($query->num_rows()==''){
			  return false;
		}else{
			  return $query->result();
            
		}   
 }
    
    
    
  function GetProductSubCat($subcat)
  {
       $this->db->from('product_master');
       $this->db->where('pro_sub_cat_id',$subcat);
       $this->db->where('status','active');
		$query=$this->db->get();

		if($query->num_rows()==''){
			  return false;
		}else{
			  return $query->result();
            
		}   
  }
    
  function GetIndividualProducts($pro_id)
  {
        $this->db->where('pro_id',$pro_id);
        $this->db->from('product_master');
		$query=$this->db->get();
		if($query->num_rows()==''){
			  return false;
		}else{
			  return $query->result();
		}  
  }

  function get_all_categories()
    {
        $this->db->select('*');
       
        $this->db->where('status','active');
        $query = $this->db->get('categories'); 
        return $query->result(); 
    }
    
    
    function get_all_subcategories()
    {
        $this->db->select('*');
      
        $this->db->where('status','active');
        $query = $this->db->get('sub_categories'); 
        return $query->result(); 
    }

    
    function  GetTestimonial(){


		$this->db->select('*');
		$this->db->where('status','active');
		$query=$this->db->get('testimonial');
		if($query->num_rows()==' '){
			return false;
		}else{
			 return $query->result();
		}   
  }
    
      
    function GetProductSearch($keywords)
    {
         $this->db->select('*');
        $this->db->from('product_master');
        $this->db->like('product_name',$keywords);
        $query = $this->db->get(); 
        return $query->result();
        
    }
    
    
    
    function get_all_brandcategories($cat_id)
    {
        $sql = "Select * from gallery_management WHERE FIND_IN_SET('".$cat_id."',image_category)";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
   
	
}