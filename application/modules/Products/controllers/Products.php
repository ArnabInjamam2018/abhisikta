<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Frontend_Controller {

  function __construct(){
		parent::__construct();
		 $this->load->model('Product_Model');
		 $this->load->library('pagination');
	}  
    
    
    
	public function index()
	{
      
        $content['categories']=$this->Product_Model->get_all_categories();
        $content['subcategories']=$this->Product_Model->get_all_subcategories();
        
        
        
        $data['ListProduct']=$this->Product_Model->GetProduct();
        
     

         $totalRecords = count($data['ListProduct']);
   
         $limit = 6;
   
 
            $config["base_url"] = base_url("Products/?");
    


    	$config["total_rows"] = $totalRecords;
        $config["per_page"] = $limit;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['num_links'] = 2;
        $config['cur_tag_open'] = '&nbsp;   <li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $this->pagination->initialize($config);
        $str_links = $this->pagination->create_links();
       
        $links = explode('&nbsp;', $str_links);
        
        $offset = 0;
        if (!empty($_GET['per_page'])) {
            $pageNo = $_GET['per_page'];
            $offset = ($pageNo - 1) * $limit;
        }
        
      
        $this->db->select("*");
        $this->db->from("product_master");
        $this->db->limit($limit, $offset);
        $cityRecords = $this->db->get();
        
        $this->load->view('include/header/header');
        $this->load->view('products', array(
            'categories'=>$content['categories'],
            'subcategories'=>$content['subcategories'],
            'totalResult' => $totalRecords,
            'ListProduct' => $cityRecords->result(),
            'links' => $links
        ));
      $this->load->view('include/footer/footer');
      $this->load->view('include/modal_master/modal_master');
        
		
	}
    
    public function detail($pro_id)
	{
        
        $content['IndividualProducts']=$this->Product_Model->GetIndividualProducts($pro_id);
		$content['subview']="product_detail";
		$this->load->view('layout/default', $content);
       
	}
    
    public function categories($cat_id)
    
    {
        
        $content['categories']=$this->Product_Model->get_all_categories();
        $content['brandcategories']=$this->Product_Model->get_all_brandcategories($cat_id);
    
        
        $content['subcategories']=$this->Product_Model->get_all_subcategories();
        
         $data['ListProduct']=$this->Product_Model->GetProductCat($cat_id);

         $totalRecords = count($data['ListProduct']);
   
         $limit = 6;
   
     
            $config["base_url"] = base_url("Products/categories/$cat_id?");
    


    	$config["total_rows"] = $totalRecords;
        $config["per_page"] = $limit;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['num_links'] = 2;
        $config['cur_tag_open'] = '&nbsp;   <li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $this->pagination->initialize($config);
        $str_links = $this->pagination->create_links();
       
        $links = explode('&nbsp;', $str_links);
        
        $offset = 0;
        if (!empty($_GET['per_page'])) {
            $pageNo = $_GET['per_page'];
            $offset = ($pageNo - 1) * $limit;
        }
        
      
        $this->db->select("*");
        $this->db->from("product_master");
        $this->db->where('cat_id',$cat_id);
        $this->db->limit($limit, $offset);
        $cityRecords = $this->db->get();
        
        $this->load->view('include/header/header');
         $this->load->view('products', array(
            'categories'=>$content['categories'],
            'brandcategories'=>$content['brandcategories'],
            'subcategories'=>$content['subcategories'],
            'totalResult' => $totalRecords,
            'ListProduct' => $cityRecords->result(),
            'links' => $links
        ));
      $this->load->view('include/footer/footer');
      $this->load->view('include/modal_master/modal_master');
        
    }
    
    
    
    
    
     public function subcategories($subcat)
    
    {
        
        $content['categories']=$this->Product_Model->get_all_categories();
        $content['subcategories']=$this->Product_Model->get_all_subcategories();
        
         $data['ListProduct']=$this->Product_Model->GetProductSubCat($subcat);

         $totalRecords = count($data['ListProduct']);
    
         $limit = 6;
 
            $config["base_url"] = base_url("Products/subcategories/$subcat?");


    	$config["total_rows"] = $totalRecords;
        $config["per_page"] = $limit;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['num_links'] = 2;
        $config['cur_tag_open'] = '&nbsp;   <li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $this->pagination->initialize($config);
        $str_links = $this->pagination->create_links();
       
        $links = explode('&nbsp;', $str_links);
        
        $offset = 0;
        if (!empty($_GET['per_page'])) {
            $pageNo = $_GET['per_page'];
            $offset = ($pageNo - 1) * $limit;
        }
        
       
        $this->db->select("*");
        $this->db->from("product_master");
        $this->db->where('pro_sub_cat_id',$subcat);
        $this->db->limit($limit, $offset);
        $cityRecords = $this->db->get();
        
        $this->load->view('include/header/header');
         $this->load->view('products', array(
            'categories'=>$content['categories'],
            'subcategories'=>$content['subcategories'],
            'totalResult' => $totalRecords,
            'ListProduct' => $cityRecords->result(),
            'links' => $links
        ));
      $this->load->view('include/footer/footer');
      $this->load->view('include/modal_master/modal_master');
        
    }
    
    
    public function product_search()
    {
        
        $keyword = $_REQUEST['keywords'];
        
      
        
        
        $data['ListProduct']=$this->Product_Model->GetProductSearch($keyword);
        
         $this->load->view('include/header/header');
        
         $this->load->view('products_search',$data );
         $this->load->view('include/footer/footer');
         $this->load->view('include/modal_master/modal_master');
        
    }




	
}
