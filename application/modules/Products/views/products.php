<section class="inner_banner">
                        <div class="banner-area bg-overlay" id="banner-area" style="background-image:url('<?php echo base_url();?>assets/images/f1.jpg');">
                           <div class="banner-heading">
                              <div class="breadcrumb">
                                    <div class="section-headline white-headline text-center">
                                        <h3>Projects</h3>
                                    </div>
                                    <ul>
                                        <li class="home-bread">Home</li>
                                        <li>Projects</li>
                                    </ul>
                                </div>
                           </div>
                        </div>
                </section>

     



        
<section class="product_list_sec" >
		<div class="services-area area-padding">
			<div class="container">
               <div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-headline text-center">
						    <h3>Our Projects</h3>

						</div>
					</div>
				</div>
                    
                <div class="row text-center">
                    <div class="col-md-3">
                       <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                           
                   <?php 
                        
                           
                           if($categories)
                           {
                               
                           
                           
                           
                           
                           for($i=0;$i<count($categories);$i++) { ?> 
                           
                          <div class="panel panel-warning">
                            <div class="panel-heading" role="tab" id="headingOne">
                              <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOne">
                                    <p onclick="choosecategories(<?php echo  $categories[$i]->cat_id; ?>)"><?php echo  $categories[$i]->cat_name; ?></p>
                                </a>
                              </h4>
                            </div>
<!--
                            <div id="collapseOne<?php echo $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                              <div class="panel-body">
                                <ul>
                                    
                             <?php for($j=0;$j<count($subcategories);$j++) {
                                    
                                       if($subcategories[$j]->cat_id == $categories[$i]->cat_id)
                                       {
                              ?>  
                                    <li><a href="<?php echo base_url('Products/subcategories/').$subcategories[$j]->sub_catid; ?>"><?php echo  $subcategories[$j]->subcat_name; ?></a></li>
                                    
                              <?php  } } ?>
                                    
                                 
                                  
                                  </ul>
                              </div>
                            </div>
-->
                          </div>
                           
                           <?php } 
                           
                           }
                           ?>
                           

                          </div>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="all-services">
                                <?php
                                if($ListProduct)
                                {
                                    
                               
                                         for($i=0;$i<count($ListProduct);$i++){?>
                                <div class="col-md-4 col-sm-4 col-xs-12" data-aos="fade-up">
                                    <div class="single-service">
                                        <a class="service-image" href="<?php echo base_url('project-details/'.$ListProduct[$i]->pro_id); ?>">
                                             <?php //echo $productlist->pro_image; 
                                                    $imgarr= (explode(",", $ListProduct[$i]->pro_image));    

                                                     ?> 
                                    
                                            
                                            <img src="<?php echo base_url('admin/webroot/uploads/product/'.$imgarr[0]); ?>" alt="">
                                        </a>
                                        <div class="service-content">
                                            <h4><?php echo $ListProduct[$i]->product_name;?></h4>
                                            <a class="service-btn" href="<?php echo base_url('project-details/'.$ListProduct[$i]->pro_id); ?>">read more</a>
                                        </div>
                                    </div>
                                </div>
                                <?php } 
                                
                                }
                                
                                ?>
                                
                            </div>
                        </div>
                    </div>
               
				</div>
                <style>
                </style>
                     <ul class="pagination pull-right">
                     
                        <?php
                        foreach ($links as $link) {
                          
                            echo "<li>" . $link . "</li>";
                        }
                        ?>
                    </ul>
			</div>
		</div>
</section>

<?php

  // printarray($brandcategories);
if($brandcategories)
{
?>




<!--
<div class="brand-area area-padding">
            <div class="test-overly"></div>
			<div class="container">
                <div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-headline text-center">
						    <h3>WE DEAL IN</h3>
						</div>
					</div>
				</div>
				<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="Reviews-content">
							<div class="brand-carousel item-indicator">
                                
                                
                             <?php for($i=0;$i<count($brandcategories);$i++) {   
                                if(count($brandcategories)>0){
                                     foreach($brandcategories as $partner){
                                         
                                   
                                   
                                ?>  
                       
                                <div class="single-testi text-center">
                                    <div class="testi-img ">
                    <img src="<?php echo site_url('admin/webroot/uploads/banner/'.$partner->image_name);?>" alt="">
                                    </div>
                                    
                                </div>
                              
                              <?php }  
                                  
                                }
                                 } 
                                
                                ?>
                              
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
-->
<?php
}
?>





<input type="hidden" id="uri_segment" value="<?php echo $this->uri->segment(2); ?>">
