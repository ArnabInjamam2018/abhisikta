
<section class="inner_banner">
    <div class="banner-area bg-overlay" id="banner-area" style="background-image:url('<?php echo base_url();?>assets/images/f1.jpg');">
       <div class="banner-heading">
          <div class="breadcrumb">
                <div class="section-headline white-headline text-center">
                    <h3>Product</h3>
                </div>
                <ul>
                    <li class="home-bread">Home</li>
                    <li>Product</li>
                </ul>
            </div>
       </div>
    </div>
</section>

     



        
<section class="product_list_sec" data-aos="fade-up">
		<div class="services-area area-padding">
			<div class="container">
               <div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="section-headline text-center">
						    <h3>Searched products</h3>

						</div>
					</div>
				</div>
              
                <div class="row text-center">
                
                    <div class="col-md-12">
                        <div class="row">
                            <div class="all-services">
                                <?php
                                if($ListProduct)
                                {
                                    
                               
                                         for($i=0;$i<count($ListProduct);$i++){?>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="single-service">
                                        <a class="service-image" href="<?php echo base_url('Products/detail/'.$ListProduct[$i]->pro_id); ?>">
                                             <?php //echo $productlist->pro_image; 
                                                    $imgarr= (explode(",", $ListProduct[$i]->pro_image));    

                                                     ?> 
                                    
                                            
                                            <img src="<?php echo base_url('admin/webroot/uploads/product/'.$imgarr[0]); ?>" alt="">
                                        </a>
                                        <div class="service-content">
                                            <h4><?php echo $ListProduct[$i]->product_name;?></h4>
                                            <a class="service-btn" href="<?php echo base_url('Products/detail/'.$ListProduct[$i]->pro_id); ?>">read more</a>
                                        </div>
                                    </div>
                                </div>
                                <?php } 
                                
                                }
                                else
                                {
                                ?>
                                
                                <h2>NO PRODUCTS FOUND</h2>
                                
                                <?php
                                }
                                
                                ?>
                                
                            </div>
                        </div>
                    </div>
               
				</div>
                <style>
                </style>

			</div>
		</div>
</section>
<input type="hidden" id="uri_segment" value="<?php echo $this->uri->segment(2); ?>">
