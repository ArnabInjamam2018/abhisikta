<input type="hidden" value="<?php echo base_url();?>" id="siteurl">

<footer class="footer-1">
            <div class="footer-area area-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="footer-content">
                                <div class="footer-head">
                                    <div class="footer-logo">
										<a href="#"><img src="<?php echo base_url('assets/logo.png'); ?>"></a>
									</div>
                                    <p><?php echo  $this->website['data']->footer_content; ?></p>                                    
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="footer-content">
                                <div class="footer-head">
                                    <h4>Quick Link</h4>
                                    <div class="footer-services-link">
                                    	 <ul class="footer-list hidden-sm">
                                            <li><a class="pagess" href="<?php echo base_url();?>">Home</a></li>
                                            <li><a class="pagess" href="<?php echo base_url('Page/aboutus'); ?>">About us</a></li>
                                            <li><a class="pagess" href="<?php echo base_url('Products'); ?>">Projects</a></li>
                                            
                                        </ul>
										<ul class="footer-list hidden-sm">
											<li><a class="pagess" href="<?php echo base_url('Services');?>">Services</a></li>
                                             <li><a href="<?php echo base_url('Client');?>">Out Client</a></li>
                                            <li><a href="<?php echo base_url('Page/contactus');?>">Contacts</a></li>
										</ul>
                                    </div>
                                    <div class="footer-icons">
                                        <h5>Follow us</h5>
										<ul>
                                            <li>
                                                <a href="<?php echo $this->website['data']->facebook_link;?>">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>
<!--
                                            <li>
                                                <a href="<?php echo $this->website['data']->twitter_link; ?>">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $this->website['data']->google_plus_link; ?>">
                                                    <i class="fa fa-google"></i>
                                                </a>
                                            </li>
-->
                                        </ul>
									</div>
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="footer-content">
                                <div class="footer-head">
                                     <h4>Address</h4>
                                    <p><b>Reg.Office :</b><?php echo  $this->website['data']->address; ?></p>
                                    <br>
                                    <p><b>Work :</b> <?php echo  $this->website['data']->corporate_address; ?></p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-area-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="copyright">
                                <p>
                                    Copyright © 2021
                                    <a href="#"></a> All Rights Reserved
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
<div class="modal fade  appointment_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
            <form method="POST" id="enquiryform">
			<div class="modal-header">
				<h5 class="modal-title mt-0" id="myLargeModalLabel">Fill The Details To Reach You</h5>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
                <div class="form-group row">
	                 <label for="example-text-input" class="col-sm-2 col-form-label">Name</label>
                 <div class="col-sm-10">
                    <input class="form-control" type="text" placeholder=" Name" id='name' name="name">
                 </div>
                </div>
                <div class="form-group row">
	                 <label for="example-text-input" class="col-sm-2 col-form-label">Contact Number</label>
                 <div class="col-sm-10">
                    <input class="form-control" type="text" placeholder="Contact Number" id='phone_number' value="" name="phone_number">
                 </div>
                </div>
                <div class="form-group row">
	                 <label for="example-text-input" class="col-sm-2 col-form-label">Email Id</label>
                 <div class="col-sm-10">
                    <input class="form-control" type="text" placeholder="Email Id" id='email' name="email">
                 </div>
                </div>
               
                <div class="form-group row">
	                 <label for="example-text-input" class="col-sm-2 col-form-label">Message</label>
                 <div class="col-sm-10">
                     <textarea class="form-control" type="text" placeholder="Message" id='message' placeholder="Message"></textarea>
                 </div>
                </div>
				
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary waves-effect waves-light"  id="sendenquiry">Enquire Now </button>
            </div>
                 <p class="success_msg" style="color:#00a63f;text-align:center;display:none">Thank your For your message.We will reach you soon.</p>
             </form>
		</div>
           
	</div>
	
</div>




        
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/popper.min.js'); ?>"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBceNl50o3BU6LrsIGJxSL9tKKvqBKIeVs"></script>
        <script src="<?php echo base_url();?>assets/js/mapcode.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/waypoints.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.stellar.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/magnific.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/venobox.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.meanmenu.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/form-validator.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script src="<?php echo base_url(); ?>assets/mainjs/userinfo.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/js/iziToast.min.js"></script> 



<script>
     $(document).ready(function(){
 $("a.collapsed").click(function(){
      $(this).find(".btn:contains('answer')").toggleClass("collapsed");
  });
          /* Aos jquery*/
  AOS.init({
    duration: 1200
  });
 });
    
    function choosecategories(cat_id)
    {
      
        var urisegment= $("#uri_segment").val();
        var uri =  urisegment.trim()
        
        var origin   = window.location.href;
        
        
      
        
       
       if(uri == "categories")
           {
               
               var url_again = origin.slice(0, origin.lastIndexOf('/'));
              ;
              
               
                 window.location.href = url_again+'/'+cat_id;
           }
        else if(urisegment == "")
            {
                
                 var   url = origin.slice(0, origin.lastIndexOf('/'));
                
              
                
                 window.location.href = url+'/Products/categories/'+cat_id;  
            }
        else if(uri == "subcategories")
            {
                
                var url_again = origin.slice(0, origin.lastIndexOf('/'));
                var url_again_new = url_again.slice(0, url_again.lastIndexOf('/'));
                
            
                window.location.href = url_again_new+'/categories/'+cat_id;  
            }
       
      
    }
    
</script>





<!-- WhatsHelp.io widget -->
<script type="text/javascript">
//    (function () {
//        var options = {
//            whatsapp: "+919123366823", // WhatsApp number
//            company_logo_url: "//static.whatshelp.io/img/flag.png", // URL of company logo (png, jpg, gif)
//            greeting_message: "Hello, how may we help you? Just send us a message now to get assistance.", // Text of greeting message
//            call_to_action: "Message us", // Call to action
//            position: "left", // Position may be 'right' or 'left'
//        };
//        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
//        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
//        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
//        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
//    })();
</script>
<!-- /WhatsHelp.io widget -->


		