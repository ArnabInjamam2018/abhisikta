<!doctype html>
<html class="no-js" lang="en">
	

<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Avisikta</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/images/logo/favicon.png">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.transitions.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/meanmenu.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/flaticon.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/icon.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/magnific.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/venobox.css">
        
        <link href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">
		<script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
	</head>
   
		<body>
        <header>
            <div class="topbar-area fix hidden-xs">
                <div class="container-fluid">
                    <div class="row">
                        <div class=" col-md-8 col-sm-6">
                            <div class="logo">
                                <a class="navbar-brand page-scroll sticky-logo" href="<?php echo base_url();?>">
                                    <img src="<?php echo base_url('admin/webroot/uploads/logo/').$this->website['data']->company_logo; ?>" alt="" style="width:70%;">
                                </a>
                            </div>
                            
                        </div>
                        <div class="col-md-4 col-sm-6">
                             <div class="topbar-right">
                           <div class="quote-button">
								 <a href="#" data-target=".appointment_modal" data-toggle="modal" class="quote-btn">Enquire Us</a>
							</div> 
                           
                                <ul>
                                   
<!--
                                    <li><a href="<?php echo $this->website['data']->google_plus_link; ?>"><i class="fa fa-google"></i></a></li>
                                    <li><a href="<?php echo $this->website['data']->twitter_link; ?>"><i class="fa fa-twitter"></i></a></li>
-->
                                    <li><a href="<?php echo $this->website['data']->facebook_link; ?>"><i class="fa fa-facebook"></i></a></li>
                                </ul> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         
            <div id="sticker" class="header-area header-area-3 hidden-xs">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                           
                            <nav class="navbar navbar-default">
                                <div class="collapse navbar-collapse" id="navbar-example">
                                    <div class="main-menu">
                                        <ul class="nav navbar-nav">
                                            <li class="<?php if($this->uri->segment(1)==""){ ?>active <?php } ?>"><a class="pagess" href="<?php echo base_url();?>">Home</a>
                                            </li>
                                            <li class="<?php if($this->uri->segment(2)=="about-us"){ ?>active <?php } ?>"  ><a class="pagess" href="<?php echo base_url('about-us'); ?>">About us</a>
                                            </li>
                                            <li class="<?php if($this->uri->segment(1)=="all-projects"){ ?>active <?php } ?>"  >
                                                <a class="pagess" href="<?php echo base_url('all-projects'); ?>">Project</a>
                                            </li>
                                           
                                            <li class="<?php if($this->uri->segment(1)=="Services"){  ?>active <?php } ?>" ><a class="pagess" href="<?php echo base_url('Services');?>">Services</a>
                                            </li>
                                    <li class="<?php if($this->uri->segment(1)=="Client"){ ?>active <?php } ?>"><a href="<?php echo base_url('Client');?>">Our Client</a></li>
                                    <li class="<?php if($this->uri->segment(1)=="Career"){ ?>active <?php } ?>"><a href="<?php echo base_url('Career'); ?>">Career</a></li>
                                            <li class="<?php if($this->uri->segment(2)=="contact-us"){ ?>active <?php } ?>"><a href="<?php echo base_url('contact-us');?>">Contact</a></li>
                                            <!-- <li class="srch_list">
                                                <div class="srchform">
                                                    <form method="POST" action="<?php echo base_url('Products/product_search'); ?>">
                                                    <input type="text" name="keywords" placeholder="search">
                                                    <button type="submit"><i class="fa fa-search"></i></button>
                                                    </form>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            
                            <div class="header-right-link">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
            <div class="mobile-menu-area hidden-lg hidden-md hidden-sm">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <div class="logo">
                                    <a href="<?php echo base_url();?>"><img src="<?php echo base_url('admin/webroot/uploads/logo/').$this->website['data']->company_logo; ?>" alt="" /></a>
                                </div>
                                <nav id="dropdown">
                                    <ul>
                                        <li><a class="pagess" href="<?php echo base_url();?>">Home</a></li>
                                        <li><a class="pagess" href="<?php echo base_url('about-us');?>">About us</a>
                                        <li><a class="pagess" href="<?php echo base_url('Products'); ?>">Project</a></li>
                                        <li><a class="pagess" href="<?php echo base_url('Services');?>">Services</a></li>
                                        <li><a href="<?php echo base_url('Client');?>">Our Client</a></li>
                                        <li class="<?php if($this->uri->segment(1)=="Career"){ ?>active <?php } ?>"><a href="<?php echo base_url('Career'); ?>">Career</a></li>
                                        <li><a href="<?php echo base_url('contact-us');?>">Contact</a></li>
                                        <!-- <li class="srch_list">
                                            <div class="srchform">
                                                <input type="text" placeholder="search">
                                                <button type="button"><i class="fa fa-search"></i></button>
                                            </div>
                                        </li> -->
                                    </ul>
                                </nav>
                            </div>					
                        </div>
                    </div>
                </div>
            </div>	
        </header>