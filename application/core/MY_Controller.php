<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

     function __construct()
	 {
		 parent::__construct();
         $this->load->model(array('MY_Model'));
         
         /*--------------Start Fetch Website data---------*/
			if($this->MY_Model->get_website_data())
			{
			$this->website['data']=$this->MY_Model->get_website_data();
			}else{
			$field=$this->MY_Model->get_field('website_setting');
			$this->website['data'] = get_instance();
			foreach($field as $key=>$val){
			$this->website['data']->$val=''; 
			}
			}
			$this->website['data']->currency_icon='<i class="fa fa-inr"></i>';
		/*--------------END Fetch Website data---------*/
		 
	 }




}
