<?php class Frontend_Controller extends MY_Controller{
	
	
    
	 
	function __construct(){
		
		parent::__construct();
		
		$this->load->helper('Common');	
		$this->load->model('MY_Model');
		if(($this->session->userdata('userinfo_login_details')!==NULL))
		{
			$this->website['loginuser']=$this->MY_Model->loginuser();
		
		}
			
		/*--------------Start Fetch Website data---------*/
		if($this->MY_Model->get_website_data())
		{
		$this->website['data']=$this->MY_Model->get_website_data();
         /*printarray($this->website['data']);*/
		}else{
		$field=$this->MY_Model->get_field('website_setting');
		$this->website['data'] = get_instance();
		foreach($field as $key=>$val){
		$this->website['data']->$val=''; 
		}
		}
		$this->website['data']->currency_icon='<i class="fa fa-inr"></i>';
		/*--------------END Fetch Website data---------*/	
		/* printarray($this->website['data']); */
		$this->CompanyName=$this->website['data']->company_name;
	    
	   
	}
	
	function authenticate()
	{
		
		$this->load->model('Token_Model');

		 $token_value=$this->Token_Model->fetch($this->Mode);	
		if(empty($token_value)) {
				$data = array(
								"ClientId" =>$this->clientId,
								"UserName" =>$this->clientuser,
								"Password" =>$this->password,
								"EndUserIp"=>$this->input->ip_address()
							 );
							 
				$url=$this->Authenticate;	
				$Service='Authenticate';
				$Type='Token';				
				$responce=TBO_Request($data,$url,$Service,$Type);	 
				if($responce->Status==1)
				{
					$data=array('token_id'=>$responce->TokenId,'date_time'=>date('Y-m-d'),'mode'=>$this->Mode);
					$this->Token_Model->insert($data);
					return $responce->TokenId;					
					
				} else {
					/*---- Error Responce ---*/
					echo $responce->Error->ErrorMessage;
				}
		} else {
			    
		        return $token_value->token_id;
			}
		
	}



	
	

}