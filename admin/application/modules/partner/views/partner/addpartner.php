<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="<?php echo site_url();?>">Home</a></li>
	<li><a href="<?php echo site_url('partner/').base64_encode("add_partner")."/".EncryptID($type);?>">Add Partner / Client</a></li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add Partner / Client</h3>
									<div class="btn-group pull-right" style="margin-right: 2%;">
									 <a href="<?php echo site_url('partner/').base64_encode('list').'/'.EncryptID($type);?>"><button class="btn btn-primary"><i class="fa fa-list-alt"></i> List</button></a>
								   </div>
                                </div>
                                <div class="panel-body">  
                                   <form id="partner_home" class="form-horizontal"  method="post" action="<?php echo site_url("partner/").base64_encode("add_partner")."/".EncryptID($type); ?>"  enctype="multipart/form-data">								
                                    
									<div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Upload  Image</label>
                                        <div class="col-md-6 col-xs-12">
                                          <input type="file" class="fileinput btn-primary" name="uploadedimages[]"  title="Browse file" required />			
										  <span class="help-block" id="image_size" ></span>
                                           <span class="help-block">This is required to upload Images</span>
                                           <span class="help-block">Images Size must be OF (100X100)pixels</span>
                                        </div>
                                    </div>
                                       
                                       
                                 <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Image for</label>
                                        <div class="col-md-6 col-xs-12">
                                            <select class="form-control " name="image_for" >
											  <option value="1">Partner</option>
											  <option value="2">Clients</option>
											  
                                            </select>
											
                                            <span class="help-block">This is required Select box</span>
                                        </div>
                                    </div>
                                       
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">status</label>
                                        <div class="col-md-6 col-xs-12">
                                            <select class="form-control " name="status" >
											  <option value="active">Active</option>
											  <option value="inactivate">Inactivate</option>
											  
                                            </select>
											
                                            <span class="help-block">This is required Select box</span>
                                        </div>
                                    </div>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-default" onclick="document.getElementById('partner_home').reset();">Clear Form</button>                                    
									<button type="submit" class="btn btn-primary pull-right">Save Changes <span class="fa fa-floppy-o fa-right"></span></button>
                                </div>
							 </form>	
                         </div>
                      </div>
                    </div>                    
                </div>				
</div>
                <!-- END PAGE CONTENT WRAPPER -->  
