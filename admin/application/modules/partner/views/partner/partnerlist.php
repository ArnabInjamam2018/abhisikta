<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="<?php echo site_url('dashboard'); ?>">Home</a></li>
    <li class="active"><a href="<?php echo site_url('partner/').base64_encode('list').'/'.EncryptID($type); ?>">Partner / Client List</a></li>

</ul>
<!-- END BREADCRUMB -->

<div class="panel panel-default">
    <!-- START HEADER SCETION -->
    <div class="panel-heading">
        <h3 class="panel-title">Partner / Client List</h3>

        <div class="btn-group pull-right" style="margin-right: 2%;">
            <a href="<?php echo site_url('partner/').base64_encode('add_partner').'/'.EncryptID($type);?>">
                <button class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add</button>
            </a>
        </div>
    </div>
    <!-- END HEADER SCETION -->
    <div class="panel-body">
        <form id="form1" method="post" action="<?php echo site_url("partner/").base64_encode('multi_partner_del').'/'.EncryptID($type);?>">
            <table id="customers2" class="table datatable">
                <thead>
                    <tr>
                        <th style="padding: 0;">
                            <label class="check">

                            </label>
                        </th>
                        <th>S.No</th>
                        <th>Partner / Client Images</th>
                        <th>Status</th>
                        <th>Image for</th>
                     
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  
					if($Partner_fatch){
					    foreach($Partner_fatch as $key=>$Partner_lists){  ?>
				                   
                        <tr>
                            <td style="padding: 0;">
                                <label class="check">

                                </label>
                            </td>
                            <td>
                                <?php echo $key+1; ?>
                            </td>
                            <td><img src="<?php echo site_url('webroot/uploads/partner').'/'.$Partner_lists->image_name;?>" alt="Smiley face" height="42" width="42" /></td>
                            <td>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-10">
                                        <label class="switch">
                                            <input type="checkbox" onClick='confirm_del("<?php echo EncryptID($Partner_lists->id);?>")' name="maintain_ration" class="maintain_ration" <?php if($Partner_lists->status =="active"){ echo "checked"; } else { } ?> value="
                                            <?php echo $Partner_lists->status ;?>"/>
                                                <span></span>
                                        </label>
                                    </div>
                                </div>
                            </td>
                            <td>
                             <?php if($Partner_lists->image_for == 1) { echo "Partners"; } else if($Partner_lists->image_for == 2) { echo "Clients"; }?>
                            
                            </td>
                            <td>
                                <?php //$id=encode_url($Partner_lists->id);?>
                                    <a class="btn btn-default btn-rounded btn-sm" href="<?php echo site_url("partner/").base64_encode('edit_partner').'/'.EncryptID($type).'/'.EncryptID($Partner_lists->id);?>"><span class="fa fa-pencil"></span></a>
                                    <a class="btn btn-danger btn-rounded btn-sm" onClick='confirm_delete("<?php echo EncryptID($Partner_lists->id);?>")'><span class="fa fa-times"></span></a>
                            </td>
                        </tr>
                        <?php  } }   ?>
                </tbody>
            </table>
        </form>
    </div>
</div>
<!-- END DATATABLE EXPORT -->

<script>
    var id = "";

    function confirm_delete(getid) {

        id = getid;
        var box = $("#mb-remove-row-single");
        box.addClass("open");
    }

    function delete_single() {
        window.location.href = "<?php echo site_url('partner/').base64_encode('delet_partner').'/'.EncryptID($type);?>/" + id;
    }
</script>

<script>
    var ids = "";

    function confirm_del(getid) {
        id = getid;
        var box = $("#mb-remove-dec");
        box.addClass("open");

    }

    function Deactive_partner() {
        window.location.href = "<?php echo site_url('partner/').base64_encode('partner_act_inactive').'/'.EncryptID($type);?>/" + id;
    }
	function page_refresh() {
        window.location.reload();
    }
</script>
<!-- START MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Removes <strong>Data</strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to remove selected row?</p>
                <p>Press Yes if you sure.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes" onClick="continue_delete()">Yes</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->

<!--START MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row-single">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Remove <strong>Data</strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to remove this row?</p>
                <p>Press Yes if you sure.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes" onClick="delete_single()">Yes</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->

<!-- START MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-dec">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span>Change Status
                <strong> Partner </strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to Change Status Banner ?</p>
                <p>Press Yes if you sure.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes" onClick="Deactive_partner()">Yes</button>
                    <button class="btn btn-default btn-lg mb-control-close"onClick="page_refresh()">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->