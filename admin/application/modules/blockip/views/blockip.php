<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title><?php echo $this->website['data']->company_name;?> </title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
      
        <!-- END META SECTION -->
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo site_url('webroot/backend') ?>/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                     
    </head>
    <body>
	 <div class="login-container lightmode">
	   <div class="animated fadeInDown" style="text-align: center;width:100%;padding-top: 100px;">
	   <h1>Your Are Not Authorized For This Section !!</h1>
	   </br>
	   <h1>Contact Admin</h1>
	 </div>
	 </div>
	    </body>
</html>