               <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('dashboard');?>">Home</a></li>
                    <li><a href="<?php echo site_url('Categories/add_categories');?>">AddCategory</a></li>
                  <li class="active">AddCategory</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                   
                                                            
                                <div class="panel panel-default tabs">                            
                                    
                                    <div class="panel-body tab-content">
                                        
                                <div class="tab-pane active" id="tab-first">
					           <br/>
                                  <form id="cat_home" class="form-horizontal" method="post" 
                                  action="<?php echo site_url("Categories/add_categories"); ?>" enctype="multipart/form-data">         
                                          
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">
                                        Category Name </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="cat_name" class="form-control"  data-validation="required"/>
                                            </div>                                            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>
                                                                        
                                                                        
                                      <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label"> Category Description</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="cat_desc" class="form-control"  data-validation="required"/>
                                            </div>                                            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>

                                          

                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label"> Meta Tag</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="meta_tag" class="form-control"  data-validation="required"/>
                                            </div>                                            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label"> Meta Description</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="meta_desc" class="form-control"  data-validation="required"/>
                                            </div>                                            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>




                                    <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Upload  Image</label>
                                        <div class="col-md-6 col-xs-12">
                                          <input type="file" class="fileinput btn-primary" name="uploadedimages[]"  title="Browse file" required />         
                                          <span class="help-block" id="image_size" ></span>
                                           <span class="help-block">This is required to upload Category Images</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">status</label>
                                        <div class="col-md-6 col-xs-12">
                                            <select class="form-control " name="status" >
                                              <option value="active">Active</option>
                                              <option value="inactivate">Inactivate</option>
                                              
                                            </select>
                                            
                                            <span class="help-block">This is required Select box</span>
                                        </div>
                                    </div>
                                          
			                    <div class="panel-footer">      
                                        <button type="button" class="btn btn-default" onclick="document.getElementById('cat_home').reset();">Clear Form</button>

                                        <button  type="submit" class="btn btn-primary pull-right" id=""   >Save <span class="fa fa-floppy-o fa-right"></span></button>
                             </div>
                                      </form>
				   
                             </div>
										
                                        
										
                                  										
				                
                                        
                                        
                                        
                                        
										
										
                                    </div>

                            
                            
                            
                        </div>
                    </div>                    
                    
                </div>
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  
<script>
$("#button_active").click(function(){
$("#postcheak_addhotel").prop('disabled', false);
});

</script>