               <!-- START BREADCRUMB -->
			  
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('dashboard');?>">Home</a></li>
                    <li><a href="javascript:void(0);">Enquiry Management</a></li>
                    <li class="active"> Enquiry List</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                           <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> Enquiry List</h3>
									  <div class="btn-group pull-right">
										<button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                        <ul class="dropdown-menu">
                                        
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'csv',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/csv.png' width="24"/> CSV</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'txt',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/txt.png' width="24"/> TXT</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/xls.png' width="24"/> XLS</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'doc',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/word.png' width="24"/> Word</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'powerpoint',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/ppt.png' width="24"/> PowerPoint</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'png',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/png.png' width="24"/> PNG</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/pdf.png' width="24"/> PDF</a></li>
                                        </ul>
                                    </div> 
									
                                </div>
                                <div class="panel-body">
								   <div class="table-responsive">
								   <form id="form1" method="post" action="<?php echo site_url("query/deleteallquery");?>">
                                    <table id="customers2" class="table table-striped table-actions datatable">
                                        <thead>
                                            <tr>

											    <th>Sr.No.</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Mobile No.</th>
                                                <th>Message</th>
                                                <th>Category</th>
                                                <th>C.V</th>
                                                <th>Date Time</th>
                                                <th>Action</th>
                                            </tr>
											
                                        </thead>
                                        <tbody>
										<?php 
										$i=1;
										if(!empty($customer_query_list)){
										foreach($customer_query_list as $key=>$cval){ 
										//printarray($cval); 
										?>				
										<tr>
									
										<td><?php echo $i; ?></td>
										<td><?php echo $cval->name; ?></td>
										<td><?php echo $cval->email; ?></td>
										<td><?php echo $cval->mobile; ?></td>
										<td> <?php if($cval->category=='Package'){
											 echo "Click On Package See More Details";
										     }else if($cval->category=='Visa'){
											 echo "Click On Visa See More Details";
										      }else{?><?php echo $cval->message; } ?></td>
										
										<td><?php if($cval->category=='Package'){?>
										   <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModa_<?php  echo $key; ?>"><?php echo $cval->category; ?></button>
										<?php } else if($cval->category=='Visa'){ ?>
											<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModa_<?php  echo $key; ?>"><?php echo $cval->category; ?></button>
										<?php } else{?>
											<?php echo $cval->category; ?>
										<?php }?></td>
										<td>
											<?php if($cval->category == "Careers"){ ?> 
											<a target="_blank" href="<?php  echo  $this->config->item('ui_url').'/assets/images/resume/'.$cval->other; ?>">Click Here</a>
											<?php } ?>
										
										</td>
										<td><?php echo $cval->create_date; ?></td>
										<td> 
										<a class="btn btn-danger btn-rounded btn-sm" onClick='confirm_delete("<?php echo $cval->id;?>")'><span class="fa fa-times"></span></a>
										</td>
										</tr>	
										<?php 
										$i++;
										} 
										} 
										?>					
                                        </tbody>
                                    </table>  
                                    </form>  
                                     							
                                     </div>
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT --> 
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER --> 

                <!-- Modal  Contect-->
				<?php 
										
				if(!empty($customer_query_list)){
				foreach($customer_query_list as $key=>$cval){ 
				$depa=unserialize($cval->other);
				
				?>	
				<div id="myModa_<?php echo $key; ?>" class="modal fade" role="dialog">
				  <div class="modal-dialog">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><?php if($cval->category=='Visa'){
							echo "Visa Query Details";
						}else{ ?> Customer Query Details <?php } ?></h4>
					  </div>
					  <div class="modal-body">
						<div class="table-responsive">
						 <table class="table table-bordered table-striped">
						 <?php if($cval->category=='Visa'){ ?>
						 <thead>
								<tr>
							  <th scope="row">First Name</th>
							  <td><?php echo $depa['FName']; ?></td>
							 
							</tr>
							<tr>
							  <th scope="row">Last Name</th>
							  <td><?php echo $depa['LName']; ?></td>
							  
							</tr>
							<tr>
							  <th scope="row">Email</th>
							  <td><?php echo $depa['EmailID']; ?></td>
							</tr>
							<tr>
							  <th scope="row">Mobile</th>
							  <td><?php echo $depa['MobileNo']; ?></td>
							</tr>
							<tr>
							  <th scope="row">Destination Country</th>
							  <td><?php echo $depa['DestinationCountry']; ?></td>
							</tr>
							<tr>
							<th scope="row">Visa Type</th>
							  <td><?php echo $depa['VisaType']; ?></td>
							</tr>
							<th scope="row">Travelers</th>
							  <td><?php echo $depa['Travelers']; ?></td>
							</tr>
							<th scope="row">Travel Date</th>
							 <td><?php echo $depa['depart_date']; ?></td>
							</tr>
							</thead>
							
						 <?php } else{ ?>
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Mobile</th>
									<th>Departure Date</th>
									<th>Package Name</th>
									<th>Message</th>
									
								</tr>
							</thead>
						 <?php } if($cval->category=='Package') {?>
						   
							<tbody>
								<tr>
									
									<td><?php echo $cval->name; ?></td>
									<td><?php echo $cval->email; ?></td>
									<td><?php echo $cval->mobile; ?></td>
									<td><?php echo $depa['deparcher_date']; ?></td>
									<td><?php echo $depa['PackageName']; ?></td>
									<td><?php echo $depa['message']; ?></td>
									
								</tr>
								
							</tbody>
				        <?php  }  ?>
						 </table>
					   </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					  </div>
					</div>
				  </div>
				</div>	
										<?php } }?>
              <!-- Modal  Contect End -->	
<!-- END PAGE CONTENT WRAPPER -->  
				<script>
				var id="";
				function confirm_delete(getid)
				{   
				    id=getid;
					var box = $("#mb-remove-row-single");
                    box.addClass("open");
				}
				function delete_single()
				{   
				window.location.href="<?php echo site_url('query/deletequery');?>/"+id;
				}
				</script>
				     <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-times"></span> Remove <strong>Data</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to remove selected row?</p>                    
                        <p>Press Yes if you sure.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <button class="btn btn-success btn-lg mb-control-yes" onClick="continue_delete()">Yes</button>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->  

		<!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row-single">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-times"></span> Remove <strong>Data</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to remove this row?</p>                    
                        <p>Press Yes if you sure.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <button class="btn btn-success btn-lg mb-control-yes" onClick="delete_single()">Yes</button>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->  
		
			  