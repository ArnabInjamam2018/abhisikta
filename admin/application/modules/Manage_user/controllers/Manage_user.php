<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Manage_user extends MY_Controller {
	
	    function __construct() {	

	    	 parent::__construct();
			 $this->table_name="user_master";
			 $this->load->model("User_Model");
			 
		}
		
	   function index() {
				
                $content['get_input']=$this->User_Model->get_input();
                $content['subview']="user_list";
				$this->load->view('layout', $content);	
		      
							
		}
		
	 function add_input() 
	 {
             
		 	 $RequestMethod = $this->input->server('REQUEST_METHOD');
         
         
        /* echo "vsfsssf";
         
         die();*/
				if($RequestMethod == "POST")
			    {     

					$user_image=$this->User_Model->user_images_upload(); 
                    
                   /* echo $user_image;
                    
                    die();*/
   					
					  $data=array(
                                    
                                    'image'=>$user_image,
                                    'user_type'=>1,
                                    'user_name'=>$this->input->post('name'),
                                    'alt_image'=>$this->input->post('alt_image'),
                                    'facebook_link'=>$this->input->post('facebook_link'),
                                    'linkedin_link'=>$this->input->post('linkedin_link'),
                                    'instagram_link'=>$this->input->post('instagram_link'),
                                    'description'=>$this->input->post('description'),
                                    'heading'=>$this->input->post('heading')             
                                   
                                
					             );

				   
		           $this->User_Model->insert_data($this->table_name,$data);
                   $this->session->set_flashdata('alert', array('message' => 'Data successfully Saved','class' => 'success'));
	              redirect('Manage_user');	
				}
				else{
				     

				   
                    $content['subview']="add_user";
                    $this->load->view('layout', $content);		
				}
				
		} 
			
			
	
function edit_input($getid)
	{

      
	  $id=$getid;
	 
	  $content['editdata']=$this->User_Model->get_data($id);
	  $content['get_role']=$this->User_Model->get_role();
	  $content['subview']="edit_user";
	  $this->load->view('layout', $content); 

       
		
	}

function update_input()
	{
		$RequestMethod=$this->input->server("REQUEST_METHOD");
    
    
    if($_FILES['uploadedimages']['error'][0]>0){
				   $user_image=$this->input->post('prev_image_multipal');
				
				   } else {
						
						$user_image=$this->User_Model->user_images_upload(); 
				       }
		if($RequestMethod == "POST"){
			
			
			$id=$this->input->post('user_id');
			
						
						$data=array(
                                    
                                    'image'=>$user_image,
                                    'user_type'=>1,
                                    'user_name'=>$this->input->post('name'),
                                    'alt_image'=>$this->input->post('alt_image'),
                                    'facebook_link'=>$this->input->post('facebook_link'),
                                    'linkedin_link'=>$this->input->post('linkedin_link'),
                                    'instagram_link'=>$this->input->post('instagram_link'),
                                    'description'=>$this->input->post('description'),
                                    'heading'=>$this->input->post('heading')               
                                   
                                    
					             );
						
						
						$this->User_Model->update_data($id,$data);	
							
						$this->session->set_flashdata('alert', array('message' => 'Updated  successfully','class' => 'success'));
			            redirect('Manage_user');
						
						
					 
		}else{
			redirect('Manage_user');
		}

	}

	/*function input_act_inactive($getid)
	{
		$id=$getid;
		$detais=$this->Slab_Model->get_data($id);
		$status=$detais[0]->status;
		if($status=='active')
		{
			$data=array('status'=>'inactivate');
			$this->Slab_Model->update_data($id,$data);	
							
		    $this->session->set_flashdata('alert', array('message' =>' status Update successfully','class' => 'success'));
			redirect('Urgency_slab');
		}else{
			$data=array('status'=>'active');
			$this->Slab_Model->update_data($id,$data);	
							
		    $this->session->set_flashdata('alert', array('message' =>' status Update successfully','class' => 'success'));
			redirect('Urgency_slab');
			
		}
	}*/
	
	         
	   
	  function delete_single_input($id){
		      
			  $this->User_Model->delete_data($id);
			  $this->session->set_flashdata('alert', array('message' => 'Row successfully deleted !','class' => 'success'));
			  redirect('Manage_user');

		}
		
	function delete_multiple_input(){
		
		   foreach ($_POST['checklist'] as $enqid){
			
				$this->User_Model->delete_data($enqid);
			  }
			  $this->session->set_flashdata('alert', array('message' => 'Row successfully deleted !','class' => 'success'));
			  redirect('Manage_user');

		}
	  

//=========================FOR CLIENT REVIEW SECTION END HERE========================================= 
}	