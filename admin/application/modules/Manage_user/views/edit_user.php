<ul class="breadcrumb">
  <li><a href="<?php echo site_url('dashboard');?>">Home</a></li>
  <li><a href="<?php echo site_url('Manage_user/edit_input');?>">Edi Team Members</a></li>
  <li class="active">Edit Team Members</li>
</ul>
<!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                <div class="panel-heading">
                                <h3 class="panel-title"><strong>Edit Team Members</h3>
                  <div class="btn-group pull-right" style="margin-right: 2%;">
                   <a href="<?php echo site_url('Manage_user');?>"><button class="btn btn-primary"><i class="fa fa-list-alt"></i> Team Members List</button></a>
                   </div>
                                </div>
                                <div class="panel-body">  
                  <?php
                                    
                                //    printarray($editdata);
                   $input=$editdata[0];
                  
                
                    ?>
                         <form id="cat_home" class="form-horizontal"  method="post" action="<?php echo site_url("Manage_user/update_input/"); ?>"  enctype="multipart/form-data">                

                             
                             
                             
                             
                               <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">
                                        Team Members Name </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="name" value="<?php echo $input->user_name ?>" class="form-control"  data-validation="required"/>
                                            </div>                                            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>
                                      
                                      
                                    <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Upload  Image</label>
                                        <div class="col-md-6 col-xs-12">
                                          <input type="file" class="fileinput btn-primary" name="uploadedimages[]"  title="Browse file"  />			
										  <span class="help-block" id="image_size" ></span>
                                           <!--<span class="help-block">This is required to upload Banner Images</span>-->
                                        </div>
                                    </div>
                             
                             
                              <?php
                                        
                        
                                       $added_product_img = $editdata[0]->image;
                        
                                       
                                    ?>
                                      <?php if(!empty($added_product_img)){?>
										 <div class="multi_imgs">
										
                                             <a href="<?php echo site_url('webroot/uploads/user_images').'/'.$added_product_img;?>"> <img src="<?php echo site_url('webroot/uploads/user_images').'/'.$added_product_img;?>" alt="Smiley face" height="100" width="150"></a>
                                             <?php
                                              }
                                              
                                                  
                                             
										  ?>  
					 					 </div>	
                                    
 <input type="hidden" name="prev_image_multipal" value="<?php echo $added_product_img; ?>"/>
                             
                             
                             
                             
                             
                              <div class="form-group">
											<label class="col-md-3 col-xs-12 control-label">Alt Image</label>
											<div class="col-md-6 col-xs-12">
											   <div class="input-group">
													<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
													<input type="text" value="<?php echo $input->alt_image ?>"  name="alt_image"  class="form-control"/>
												</div>   
											</div>
										</div> 
										

                                   <div class="form-group">
											<label class="col-md-3 col-xs-12 control-label">Facebook Link </label>
											<div class="col-md-6 col-xs-12">
											   <div class="input-group">
													<span class="input-group-addon"><i class="fa fa-facebook"></i></span>
													<input type="text" value="<?php echo $input->facebook_link ?>"  name="facebook_link"  class="form-control"/>
												</div>   
											</div>
										</div> 
										
										<div class="form-group">
											<label class="col-md-3 col-xs-12 control-label">Linkedin Link</label>
											<div class="col-md-6 col-xs-12">
											   <div class="input-group">
													<span class="input-group-addon"> <i class="fa fa-linkedin"></i> </span>
													<input type="text" name="linkedin_link"  value="<?php echo $input->linkedin_link ?>"  class="form-control"/>
												</div>   
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-3 col-xs-12 control-label">Instagram Link</label>
											<div class="col-md-6 col-xs-12">
												 <div class="input-group">
													<span class="input-group-addon"> <i class="fa fa-instagram"></i> </span>
													<input type="text" name="instagram_link"  value="<?php echo $input->instagram_link ?>"  class="form-control"/>
												</div>   
											</div>
										</div>
                                      
                                      
                                       <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Heading </label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="heading" value="<?php echo $input->heading ?>" class="form-control" />
                                            </div>            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>
									<div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Description</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                               <!-- <input type="text" name="image_url_link" class="form-control" />-->
                     <textarea name="description" class="form-control summernote"><?php echo $input->description ?></textarea>
                                            </div>            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>
                                                        
 

                                <input type="hidden" name="user_id" value="<?php echo $input->user_id; ?>" />
                             

                                <div class="panel-footer">
                                    <button type="button" class="btn btn-default" onclick="document.getElementById('cat_home').reset();">Clear Form</button>                                    
                  <button type="submit" class="btn btn-primary pull-right">Save Changes <span class="fa fa-floppy-o fa-right"></span></button>
                </div>
               </form>
                </div>
            </div>
          </div>                    
          </div>
       </div>