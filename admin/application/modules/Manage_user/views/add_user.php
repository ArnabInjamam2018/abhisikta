               <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('dashboard');?>">Home</a></li>
                    <li><a href="<?php echo site_url('Manage_user/add_input');?>">Add Team Members</a></li>
                  <li class="active">Add Team Members</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                   
                                                            
                                <div class="panel panel-default tabs">                            
                                    <!--<ul class="nav nav-tabs" role="tablist">
                                        <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Gentlemen Drycleaning Category </a></li>
                                        <li ><a href="#tab-second" role="tab" data-toggle="tab">Women DryCleaning Category </a></li>
                                        <li><a href="#tab-third" role="tab" data-toggle="tab">Kids Dry Cleaning Category</a></li>
                                        <li><a href="#tab-four" role="tab" data-toggle="tab">Household Drycleaning Category</a></li>
                                        <li><a href="#tab-five" id="button_active" role="tab" data-toggle="tab">Laundry</a></li>
                                    </ul>-->
                                    <div class="panel-body tab-content">
                                        
                                <div class="tab-pane active" id="tab-first">
					           <br/>
                                  <form id="input_home" class="form-horizontal" method="post" 
                                  action="<?php echo site_url("Manage_user/add_input"); ?>" enctype="multipart/form-data">

                                  <!-- <div class="form-group">
                                      <label class="col-md-3 col-xs-12 control-label" >Role</label>
                                         <div class="col-md-6 col-xs-12">
                                             <select  data-placeholder="Select" name="role" class="form-control select" tabindex="2" required>
                                             
                                               <option value="">Select</option>
                                               <?php foreach($get_role as $role)
                                                 { ?>
                                                  <option value="<?php echo $role->id;?>" > <?php echo $role->role_name;?> </option>
                                             
                                              <?php } ?>
                                            </select>
                                         </div>           
                                    </div>  -->      
                                          
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">
                                        Team Member Name </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="name" class="form-control"  data-validation="required"/>
                                            </div>                                            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>
                                      
                                      
                                    <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Upload  Image</label>
                                        <div class="col-md-6 col-xs-12">
                                          <input type="file" class="fileinput btn-primary" name="uploadedimages[]"  title="Browse file"  />			
										  <span class="help-block" id="image_size" ></span>
                                           <!--<span class="help-block">This is required to upload Banner Images</span>-->
                                        </div>
                                    </div>
                                      
                                      
                                      
                                     <div class="form-group">
											<label class="col-md-3 col-xs-12 control-label">Alt Image</label>
											<div class="col-md-6 col-xs-12">
											   <div class="input-group">
													<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
													<input type="text"   name="alt_image"  class="form-control"/>
												</div>   
											</div>
										</div> 

                                   <div class="form-group">
											<label class="col-md-3 col-xs-12 control-label">Facebook Link </label>
											<div class="col-md-6 col-xs-12">
											   <div class="input-group">
													<span class="input-group-addon"><i class="fa fa-facebook"></i></span>
													<input type="text" name="facebook_link"  class="form-control"/>
												</div>   
											</div>
										</div> 
										
										<div class="form-group">
											<label class="col-md-3 col-xs-12 control-label">Linkedin Link</label>
											<div class="col-md-6 col-xs-12">
											   <div class="input-group">
													<span class="input-group-addon"> <i class="fa fa-linkedin"></i> </span>
													<input type="text" name="linkedin_link"  class="form-control"/>
												</div>   
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-3 col-xs-12 control-label">Instagram Link</label>
											<div class="col-md-6 col-xs-12">
												 <div class="input-group">
													<span class="input-group-addon"> <i class="fa fa-instagram"></i> </span>
													<input type="text" name="instagram_link"  class="form-control"/>
												</div>   
											</div>
										</div>
                                      
                                      
                                       <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Heading </label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="heading" class="form-control" />
                                            </div>            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>
									<div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Description</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                               <!-- <input type="text" name="image_url_link" class="form-control" />-->
                             <textarea name="description" class="form-control summernote"></textarea>
                                            </div>            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>
                                                        
                                                          
                                                                        
                                                                        
                                          
			                    <div class="panel-footer">      
                                        <button type="button" class="btn btn-default" onclick="document.getElementById('input_home').reset();">Clear Form</button>

                                        <button  type="submit" class="btn btn-primary pull-right" id=""   >Save <span class="fa fa-floppy-o fa-right"></span></button>
                             </div>
                                      </form>
				   
                             </div>
										
                                        
										
                                  										
				                
                                        
                                        
                                        
                                        
										
										
                                    </div>
<!--                                    <div class="panel-footer">      
                                        <button type="button" class="btn btn-default" onclick="document.getElementById('add_hotel').reset();">Clear Form</button>

                                        <button  type="submit" class="btn btn-primary pull-right" id="postcheak_addhotel"   disabled>Save <span class="fa fa-floppy-o fa-right"></span></button>
                                    </div>-->
                            
                            
                            
                        </div>
                    </div>                    
                    
                </div>
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  
<script>
$("#button_active").click(function(){
$("#postcheak_addhotel").prop('disabled', false);
});

</script>