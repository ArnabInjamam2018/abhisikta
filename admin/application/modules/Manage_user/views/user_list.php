               <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('dashboard');?>">Home</a></li>
                    <li><a href="<?php echo site_url('Manage_user');?>">Team Members     List</a></li>
                    <li class="active">Team Members List</li>
                </ul>
                <!-- END BREADCRUMB -->
                <?php
				//printarray($package_list);
				?>
                 <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                             <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Team Members List</h3>
									   <div class="btn-group pull-right" style="margin-right: 2%;">
										<!--<button class="btn btn-danger" onClick="ischeckbox()"><i class="fa fa-trash-o"></i> Remove</button>-->
									  </div>
									  <div class="pull-right" style="margin-right: 2%;">
										<a href="<?php echo site_url('Manage_user/add_input');?>"><button class="btn btn-danger" style="background-color:black;"><i class="glyphicon glyphicon-plus-sign"></i> Add Team Members</button></a>
									  </div>
                                </div>
                                
                                <?php
                                
                               //   printarray($input_lists);
                                ?>
                                <div class="panel-body">
								   <div class="table-responsive">
								   <form id="form1"  method="post" action="<?php echo site_url("Manage_user/delete_multiple_input");?>">
                                    <table id="customers2" class="table table-striped table-actions datatable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
											    <th style="padding: 0;">
                                              <!--  <label class="check"><input type="checkbox" name="check_all" id="selectall" class=""/></label>-->
                                                </th>
											  <!--  <th>Role</th>-->
												<th>Team Members Name</th>
                                                <th>Image</th>
                                                <th>Linkedin</th>
                                                <th>Facebook Link</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php if(!empty($get_input) ) {  ?>
										<?php foreach($get_input as $key=>$input_lists):?>
                                        <tr>
                                               
                                                <td><?php echo $key+1;?></td>
											   	<td style="padding: 0;"><label class="check">
                                                   <!-- <input type="checkbox" name="checklist[]" value="<?php echo $input_lists->user_id; ?>" class="checkbox1"/>-->
                                                    </label></td>
                                               <!--  <td><?php foreach($get_role as $key=>$role)
                                                          { 
                                                            if($role->id==$input_lists->user_role)
                                                            {
                                                                echo $role->role_name;
                                                            }

                                                          }?>
                                                    
                                                </td>-->
                                                <td><?php echo $input_lists->user_name;?></td>
                                                <td> <img src="<?php echo base_url('webroot/uploads/user_images/').$input_lists->image; ?>" width="100" height="100"></td>
                                                <td><?php echo $input_lists->linkedin_link;?></td>
                                                <td><?php echo $input_lists->facebook_link;?></td>


                                                <td>
												 <?php $id=$input_lists->user_id;?>
												<a class="btn btn-default btn-rounded btn-sm" href="<?php echo site_url("Manage_user/edit_input/$id");?>" ><span class="fa fa-pencil"></span></a>
                                                 <?php $packagekey=$input_lists->user_id;?>
												<a class="btn btn-danger btn-rounded btn-sm" onClick='confirm_delete("<?php echo $packagekey;?>")'><span class="fa fa-times"></span></a>
												</td>

                                            </tr>
                                          <?php endforeach;?>
										<?php } ?>
                                        </tbody>
                                    </table>  
                                      </form>									
                                     </div>
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT --> 
                          
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->
				
				<script>
				var id="";
				function confirm_delete(getid)
				{   
				   id=getid;
					 var box = $("#mb-remove-row-singles");
                     box.addClass("open");
				}
				function delete_single()
				{   
				window.location.href="<?php echo site_url('Manage_user/delete_single_input');?>/"+id;
				}
				</script>
				
			<script>
    var ids = "";

    function confirm_del(getid) {
        id = getid;
        var box = $("#mb-remove-dec");
        box.addClass("open");

    }

    function Deactive_category() {
        window.location.href = "<?php echo site_url('Manage_user/input_act_inactive');?>/" + id;
    }
    function page_refresh() {
        window.location.reload();
    }
</script>
				     <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-times"></span> Remove <strong>Data</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to remove selected row?</p>                    
                        <p>Press Yes if you sure.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <button class="btn btn-success btn-lg mb-control-yes" onClick="continue_delete()">Yes</button>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->  
     <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-dec">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span>Change Status
                <strong> Category </strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to Change Status Category ?</p>
                <p>Press Yes if you sure.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes" onClick="Deactive_category()">Yes</button>
                    <button class="btn btn-default btn-lg mb-control-close"onClick="page_refresh()">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
		<!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row-singles">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-times"></span> Remove <strong>Data</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to remove this row?</p>                    
                        <p>Press Yes if you sure.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <button class="btn btn-success btn-lg mb-control-yes" onClick="delete_single()">Yes</button>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->