<?php class User_Model extends MY_Model{
	
	function __construct() {

		parent::__construct();
		
	}

	  public function get_input(){
	
		$this->db->select('*');
		$this->db->order_by("user_id","desc");
           $this->db->where('user_type',1);
		$query=$this->db->get('user_master');

		   if($query->num_rows() ==''){
				return '';
				}else{
				return $query->result();
				
			}
	}

	public function get_role()
	{

		$query=$this->db->get('role_master');
		if($query->num_rows ==" "){
			return false;
		}else{
			return $query->result();
		}

	}

	function insert_data($table,$data)
	{
              $this->db->insert($table,$data);
    }
	
	function update_data($id,$data)
	{         
              $this->db->where('user_id',$id);
              $this->db->update('user_master',$data);
    }
	
	function delete_data($id)
	{
                  $this->db->where('user_id',$id);
		          $this->db->delete('user_master');
    }
	
	function get_data($id)
	{
		$this->db->where('user_id',$id);
		$query=$this->db->get('user_master');
		if($query->num_rows ==" "){
			return false;
		}else{
			return $query->result();
		}
	}


	 function checkemail($email)
	{

         
		
	    $this->db->where('user_email', $email);
            $query=$this->db->get('user_master');
	    
		if($query->num_rows() == 0)
	    {
		return "valid";
	    }
	    else if($query->num_rows() == 1)
	    {
		
                return "alreadyexistsemail";
            
	    }
	}
    
    
    
	function user_images_upload()
	{
	$name_array= array();
    $number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);
    $files = $_FILES['uploadedimages'];
    $errors = array();
    for($i=0;$i<$number_of_files;$i++)
    {
      if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
    }
    if(sizeof($errors)==0)
    {
      $this->load->library('upload');
      $config['upload_path'] = FCPATH . '/webroot/uploads/user_images';
      $config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';
      for ($i = 0; $i < $number_of_files; $i++){
        $_FILES['uploadedimage']['name'] = $files['name'][$i];
        $_FILES['uploadedimage']['type'] = $files['type'][$i];
        $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
        $_FILES['uploadedimage']['error'] = $files['error'][$i];
        $_FILES['uploadedimage']['size'] = $files['size'][$i];
        $this->upload->initialize($config);
        if ($this->upload->do_upload('uploadedimage'))
        {
           $data['uploads'][$i] = $this->upload->data();
		    $name_array[] = $data['uploads'][$i]['file_name'];
			
			if("TRUE"=="TRUE"){
		   	$config['image_library'] = 'gd2';
			$config['source_image']  = $data['uploads'][$i]['full_path'];
			//if($this->dev['categories_maintain_ratio']=="TRUE") { $maintain_ratio=TRUE; } else { $maintain_ratio=FALSE; } 
            $config['maintain_ratio'] = false;
			$config['quality'] = '100%';
			/*$config['width']	 = 1351;
			$config['height']	= 500;*/
			$this->image_lib->initialize($config); 
		    $this->load->library('image_lib', $config); 
            $this->image_lib->resize();
			}
        }
        else
        {
          $data['upload_errors'][$i] = $this->upload->display_errors();
		  $errors[]=$data['upload_errors'][$i];
		  print_r ($this->upload->display_errors());
		  return FALSE;
        }
      }
    }
    else
    {
     
    }	
	
	 return $names= implode(',', $name_array);
	}

//======================= FOR CLIENT REVIEW SECTION END HERE=========================================
   
}