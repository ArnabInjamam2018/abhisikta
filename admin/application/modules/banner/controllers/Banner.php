<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Banner extends MY_Controller {
	function __construct() {
	 parent::__construct();
	 
	 $this->load->model('Banner_Model');
	}
	public function index()
	{
		$content['Banner_fatch']=$this->Banner_Model->all_fatch_banner();
         $content['subview']="banner/banner/bannerlist";
		 $this->load->view('layout', $content);
	}
	
	function add_banner()
	{
		$RequestMethod=$this->input->server("REQUEST_METHOD");
		 if($RequestMethod == "POST"){
			 
			$banner_img_name=$this->Banner_Model->banner_images_upload(); 
			$data=array(
			'image_seo_title'=>$this->input->post('image_seo_title'),
			'image_url_link'=>$this->input->post('image_url_link'),
			'image_name'=>$banner_img_name,
			'status'=>$this->input->post('status')
			
			);
			$this->Banner_Model->insert_banner($data);
			$this->session->set_flashdata('alert', array('message' => 'Add banner successfully','class' => 'success'));
			redirect('banner');
		 }else{
		$content['subview']="banner/banner/addbanner";
		$this->load->view('layout', $content);
		 }
		
	}
	
	function delet_banner($getid)
	{
		 $id=decode_url($getid);
		 $get_img=$this->Banner_Model->get_banner($id);
		
		 if($get_img){
			 $img_name=$get_img[0]->image_name;
			 $id=$get_img[0]->id;
			 $img_file=FCPATH . '/webroot/uploads/banner/'.$img_name;
			 if(!unlink($img_file)) {} else { }
			 $this->Banner_Model->delet_banner_single($id);
			 $this->session->set_flashdata('alert', array('message' => 'Deleted banner successfully','class' => 'success'));
			 redirect('banner');
			 
		 }else{
			 $this->session->set_flashdata('alert', array('message' => 'Deleted banner successfully','class' => 'error'));
			 redirect('banner');
			 
		 }
		 
	}
	
	function multi_banner_del()
	{
		$RequestMethod=$this->input->server('REQUEST_METHOD');
		if($RequestMethod== 'POST'){
			
			foreach($_POST['checklist'] as $id){
				$get_img=$this->Banner_Model->get_banner($id);
				$img_name=$get_img[0]->image_name;
			    $id=$get_img[0]->id;
			    $img_file=FCPATH . '/webroot/uploads/banner/'.$img_name;
			    if(!unlink($img_file)) {} else { }
			    $this->Banner_Model->delet_banner_single($id);
				
			}
			$this->session->set_flashdata('alert', array('message' => 'Add banner successfully','class' => 'success'));
			 redirect('banner');
		}else{
			$this->session->set_flashdata('alert', array('message' => 'Not valid page','class' => 'error'));
			redirect('banner');
		}
	
		
	}
	
	function edit_banner($getid)
	{
	  $id=decode_url($getid);
	  $content['banner_edit']=$this->Banner_Model->get_banner($id);
	  $content['subview']="banner/banner/editbanner";
	  $this->load->view('layout', $content); 
		
	}
	function update_banner()
	{
		$RequestMethod=$this->input->server("REQUEST_METHOD");
		if($RequestMethod == "POST"){
			
			
			$id=$this->input->post('banner_id');
			if($_FILES['uploadedimages']['error'][0]>0){
				   $banner_img_name=$this->input->post('prev_image');
				
				   } else {
						$banner_detais=$this->Banner_Model->get_banner($id);
						$previous_name=$banner_detais[0]->image_name;
						$p_id=$banner_detais[0]->id; 
						$img_file=FCPATH . '/webroot/uploads/banner/'.$previous_name;
						if (!unlink($img_file)) {} else { }
						$banner_img_name=$this->Banner_Model->banner_images_upload(); 
				       }
						
						$data=array(
						'image_seo_title'=>$this->input->post('image_seo_title'),
						'image_url_link'=>$this->input->post('image_url_link'),
						'image_name'=>$banner_img_name,
						'status'=>$this->input->post('status')
			
			              );
						
						$this->Banner_Model->update_banners($id,$data);	
							
						$this->session->set_flashdata('alert', array('message' => 'Update banner successfully','class' => 'success'));
			            redirect('banner');
						
						
					 
		}else{
			redirect('banner');
		}

	}

	function banner_act_inactive($getid)
	{
		$id=$getid;
		$banner_detais=$this->Banner_Model->get_banner($id);
		$status=$banner_detais[0]->status;
		if($status=='active')
		{
			$data=array('status'=>'inactivate');
			$this->Banner_Model->update_banners($id,$data);	
							
		    $this->session->set_flashdata('alert', array('message' =>'Banner status Update successfully','class' => 'success'));
			redirect('banner');
		}else{
			$data=array('status'=>'active');
			$this->Banner_Model->update_banners($id,$data);	
							
		    $this->session->set_flashdata('alert', array('message' =>'Banner status Update successfully','class' => 'success'));
			redirect('banner');
			
		}
	}
	
	
	
}	