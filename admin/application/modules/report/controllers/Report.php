<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Report extends MY_Controller {
	function __construct() {
	parent::__construct();
	$this->load->helper('report');
	$this->load->model('Report_Model');
	}
	public function index()
	{
		
	$content['getairlinelist']=$this->Report_Model->getairlinelist('B2C');
	$content['gethotellist']=$this->Report_Model->gethotellist('B2C');
	$content['subview']="report/reports";
	$this->load->view('layout', $content);
	}
	
	
	public function report_b2b()
	{
	$content['getairlinelist']=$this->Report_Model->getairlinelist('B2B');
	$content['gethotellist']=$this->Report_Model->gethotellist('B2B');
	$content['subview']="report/reports_b2b";
	$this->load->view('layout', $content);
	}
	
	function getreportforflight()
	{		
		$getreportlist=$this->Report_Model->get_range_data_flight($_GET,'B2C');
		$content['getreportlist']=$getreportlist;
		$content['subview']="report/flight_reportlist";
		$this->load->view('layout', $content);
	}
	
	function getflightreportb2b()
	{
		/* echo "dd";die; */
	$getreportlist=$this->Report_Model->get_range_data_flight($_GET,'B2B');
	$content['getreportlist']=$getreportlist;
	$content['subview']="report/flight_reportlist_b2b";
	$this->load->view('layout', $content);	
		
	}
	
	function getreportforhotel()
	{		
		$getreportlist=$this->Report_Model->get_range_data_hotel($_GET);
		$content['getreportlist']=$getreportlist;
		$content['subview']="report/hotel_reportlist";
		$this->load->view('layout', $content);
	}
	
	
	
}	