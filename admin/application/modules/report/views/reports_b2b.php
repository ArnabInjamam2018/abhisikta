               <!-- START BREADCRUMB -->
			  
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('dashboard');?>">Home</a></li>
                    <li><a href="javascript:void(0);">Report Management</a></li>
                    <li class="active">Sales Report B2B</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                           <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Sales Report B2B</h3>                          
                                </div>
                                <div class="panel-body">
								
								<form method="GET" action="<?php echo site_url('report/getflightreportb2b'); ?>" >
								  <div class="row">
								  
								  <div class="col-md-12" >
								  <h3>Sales Report For Flight</h3>
								   <div class="col-md-3" >
								   Date range (Min)
								  <input type="text" name="from" class="form-control" id="mindatepicker_flight">
								  </div>
								   <div class="col-md-3" >
								   Date range (Max)
								   <input type="text" name="to" class="form-control" id="maxdatepicker_flight" >
								  </div>
								   <div class="col-md-3" >
								   Select Airline
								   <?php //printarray($getairlinelist); ?>
								  <select name="airline" class="form-control">
								  <option value="" >Select Airline</option>
								  <option value="All" >All</option>
								  <?php foreach($getairlinelist as $getval){ ?>
								  <option value="<?php echo $getval->airlinecode; ?>"><?php echo $getval->airlinename; ?></option>
								  <?php } ?>
								  </select>
								  </div>
								  <div class="col-md-3" >
								  <p></p>
								  <input type="submit" value="Get Report"  class="btn btn-primary">
								  </div>
								  </div>
								  </div>
								</form>  
                                </div>
								<div class="panel-body">
								
								<form method="GET" action="<?php echo site_url('report/getreportforhotel'); ?>" >
								  <div class="row">
								  <div class="col-md-12" >
								  <h3>Sales Report For Hotel</h3>
								   <div class="col-md-3" >
								   Date range (Min)
								  <input type="text" name="from" class="form-control" id="mindatepicker_hotel">
								  </div>
								   <div class="col-md-3" >
								   Date range (Max)
								   <input type="text" name="to" class="form-control" id="maxdatepicker_hotel" >
								  </div>
								   <div class="col-md-3" >
								   Select Source (Destination)
								   <?php //printarray($gethotellist); ?>
								  <select name="source" class="form-control">
								  <option value="All" >All</option>
								  <?php foreach($gethotellist as $getval){ ?>
								  <option value="<?php echo $getval->origin; ?>"><?php echo $getval->origin; ?></option>
								  <?php } ?>
								  </select>
								  </div>
								  <div class="col-md-3" >
								  <p></p>
								  <input type="submit" value="Get Report"  class="btn btn-primary">
								  </div>
								  </div>
								  </div>
								</form>  
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT --> 
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER --> 
 
				<script>
  $( function() {
    $( "#mindatepicker_flight" ).datepicker({});
    $( "#mindatepicker_hotel" ).datepicker({});
    $( "#maxdatepicker_flight" ).datepicker({});
    $( "#maxdatepicker_hotel" ).datepicker({});
  } );
  </script>