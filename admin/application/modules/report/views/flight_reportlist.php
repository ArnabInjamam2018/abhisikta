               <!-- START BREADCRUMB -->			  
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('dashboard');?>">Home</a></li>
                    <li><a href="javascript:void(0);">Report Management</a></li>
                    <li class="active">Sales Report List B2C (Flight) </li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                           <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Sales Report List B2C (Flight)</h3>
									  <div class="btn-group pull-right">
										<button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                                        <ul class="dropdown-menu">
                                        
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'csv',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/csv.png' width="24"/> CSV</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'txt',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/txt.png' width="24"/> TXT</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/xls.png' width="24"/> XLS</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'doc',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/word.png' width="24"/> Word</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'powerpoint',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/ppt.png' width="24"/> PowerPoint</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'png',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/png.png' width="24"/> PNG</a></li>
                                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img src='<?php echo site_url('webroot/backend') ?>/img/icons/pdf.png' width="24"/> PDF</a></li>
                                        </ul>
                                    </div> 
									
									
                                                                     
                                    
                                </div>
                                <div class="panel-body">
								   <div class="table-responsive">
								  
                                    <table id="customers2" class="table table-striped table-actions datatable">
                                        <thead>
                                            <tr>
											    <th>Sr.No.</th>
                                                <th>Booking Id</th>
												<th>Payment Method</th>
                                                <th>Airline Name</th>
                                                <th>Airline Code</th>
                                                <th>Admin Net Fare</th>
                                                <th>User Net Fare</th>
                                                <th>Net Profit</th>
                                                <th>Date Time</th>                                      
                                            </tr>
											
                                        </thead>
                                        <tbody>
<?php 
$i=1;
if(!empty($getreportlist)){
foreach($getreportlist as $getval){ 
/* printarray($getval); */
?>				
<tr>
<td><?php echo $i; ?></td>
<td><a href="<?php echo site_url('flight/bookingdetailsb2c/'.$getval->package_ref_id.''); ?>" target="_blank"><span class="label label-success" style="font-size: 100%;cursor: pointer;"><?php echo $getval->package_ref_id; ?></span></a></td>
<td><?php echo $getval->payment_method;?></td>
<td><?php echo $getval->airlinename;?></td>
<td><?php echo $getval->airlinecode;?></td>
<td><input type="hidden" class="adminnetfare" value="<?php echo $getval->adminnetfare;?>" ><?php echo $this->website['data']->currency_icon.' '.number_format($getval->adminnetfare);?></td>
<td><input type="hidden" class="usernetfare" value="<?php echo $getval->usernetfare;?>" ><?php echo $this->website['data']->currency_icon.' '.number_format($getval->usernetfare);?></td>
<td><input type="hidden" class="netprofit" value="<?php echo $getval->netprofit;?>" ><?php echo $this->website['data']->currency_icon.' '.number_format($getval->netprofit);?></td>
<td><?php echo $getval->datetime;?></td>                                              
</tr>	
<?php 
$i++;
} 
}
?>	
				
                                        </tbody>
<tfoot>
     <tr>
											    <th></th>
                                                <th></th>
												<th colspan="2">Total Admin Net Fare : <?php echo $this->website['data']->currency_icon;?> <span id="adminnetfare"></span></th>
                                                <th colspan="2" >Total User Net Fare: <?php echo $this->website['data']->currency_icon;?> <span id="usernetfare"></span></th>
                                                <th colspan="2" >Total Net Profit: <?php echo $this->website['data']->currency_icon;?> <span id="countnetprofit"></span></th>
                                                <th></th>                                      
                                            </tr>
  </tfoot>										
                                    </table>  
                                     							
                                     </div>
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT --> 
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  
<script>
$(document).ready(function() {
    var countnetprofit = 0;
    var adminnetfare = 0;
    var usernetfare = 0;
    $('.netprofit').each(function() {
        var netprofit = $(this);
        var q = netprofit.closest('tr').find('.adminnetfare').val();
        var g = netprofit.closest('tr').find('.usernetfare').val();
        countnetprofit += parseInt(netprofit.val());
		adminnetfare += parseInt(q);
        usernetfare += parseInt(g);
    });
	$('#adminnetfare').html(adminnetfare);
    $('#usernetfare').html(usernetfare);
    $('#countnetprofit').html(countnetprofit); 
});
</script>				