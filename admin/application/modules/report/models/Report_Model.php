<?php class Report_Model  extends MY_Model{
	
	public function getairlinelist($b_type)
    {	 
 
		  $query=$this->db->select('airlinename, airlinecode')
						  ->from('sales_report_flight')             
                          ->group_by('airlinename')
						  ->order_by("id","desc")
						  ->where('business_type',$b_type)
                          ->get();
			if($query->num_rows() ==''){
			return false;
			}else{
			return $query->result();				
			}
	   
	}
	
	public function gethotellist($b_type)
    {	 
 
		  $query=$this->db->select('origin')
						  ->from('sales_report_hotel')             
                          ->group_by('origin')
						  ->where('business_type',$b_type)
                          ->get();
			if($query->num_rows() ==''){
			return false;
			}else{
			return $query->result();				
			}
	   
	}
	
	function get_range_data_flight($data,$btype)
	{
		$this->db->select('*');
		$this->db->where('business_type',$btype);
		if(!empty($data['airline'])){
		if($data['airline']!='All'){
		$this->db->where('airlinecode',$data['airline']);	
		}
		}
		if(!empty($data['from'])){
		$this->db->where('datetime >=',''.changedate($data['from']).' 00:00:00');
		}
		if(!empty($data['to'])){
		$this->db->where('datetime <=',''.changedate($data['to']).' 24:59:59');
		}		
		$query=$this->db->get('sales_report_flight');
		return $query->result();
	}
	
	function get_range_data_hotel($data)
	{
		$this->db->select('*');
		
		if($data['source']!='All'){
		$this->db->where('origin',$data['source']);	
		}
		
		if(!empty($data['from'])){
		$this->db->where('datetime >=',''.changedate($data['from']).' 00:00:00');
		}
		if(!empty($data['to'])){
		$this->db->where('datetime <=',''.changedate($data['to']).' 24:59:59');
		}		
		$query=$this->db->get('sales_report_hotel');
		return $query->result();
	}
	
}	