               <!-- START BREADCRUMB -->


                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('dashboard');?>">Home</a></li>
                    <li><a href="<?php echo site_url('Productmanagement/add_products');?>">Products</a></li>
                  <li class="active">AddProducts</li>
                </ul>
                <!-- END BREADCRUMB -->
                   <input type="hidden" id="base_url" value="<?php echo base_url();?>">
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
				        <br/>
                            <form id="cat_home" class="form-horizontal" method="post" 
                                  action="<?php echo site_url("Productmanagement/add_products"); ?>" enctype="multipart/form-data"> 
                                     
                                    
                                    
                                  <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Add Category</label>
                                        <div class="col-md-6 col-xs-12">
                                            <select class="form-control" name="cat_id" id="cat_id" onchange="catwisesubcat(this.value);">
                                                 <option selected disabled value="0">Select Category</option>
                                                <?php for ($i=0;$i<count($categories);$i++){?>
                                                <option value="<?php echo $categories[$i]->cat_id;?>"><?php echo $categories[$i]->cat_name;?></option>
                                                <?php } ?>
                                            
                                            </select>
                                            
                                            <span class="help-block">This is required Select box</span>
                                        </div>
                                    </div> 
                                
                                <!-- <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Add Subcategory</label>
                                        <div class="col-md-6 col-xs-12">
                                            <select class="form-control" name="subcat_id" id="subcat_id" onchange="subcat_wise_subsub(this.value);" >
                                                 <option selected value="0">Select Subcategory</option>
                                                <!--<?php for ($i=0;$i<count($sub_cat_list);$i++){?>
                                                <option value="<?php echo $sub_cat_list[$i]->subcat_id;?>"><?php echo $sub_cat_list[$i]->subcat_name;?></option>
                                                <?php } ?>
                                            
                                            </select>
                                            

                                        </div>
                                </div>  -->
                                
                             
								
								
								
                                  <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">
                                        Project Name </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="product_name" class="form-control"  data-validation="required"/>
                                            </div>                                            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>
                                                                        
                                                                        
                                      <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label"> Project Description</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <textarea type="text" name="product_description" class="form-control"  data-validation="required"> </textarea>
                                            </div>                                            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                      </div>
                            
<!--
                                     <div class="form-group" >
                                        <label class="col-md-3 col-xs-12 control-label">
                                        Extra Data </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <textarea name="product_code" class="form-control summernote"></textarea>
                                            </div>                                            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                     </div>
-->
                                      



                                    <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Upload  Image</label>
                                        <div class="col-md-6 col-xs-12">
                                          <input type="file" class="fileinput btn-primary" name="uploadedimages[]"  title="Browse file" required multiple/>         
                                          <span class="help-block" id="image_size" ></span>
                                           <span class="help-block">This is required to upload Product Images</span>
                                        </div>
                                    </div>
                                
                                 

                                    <div class="form-group" style="display:none;">
                                        <label class="col-md-3 col-xs-12 control-label">Others</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="others" class="form-control" />
                                            </div>                                            
                                            <span class="help-block">This is required text field</span>
                                        </div>
                                    </div>

<!--
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">For Home</label>
                                        <div class="col-md-6 col-xs-12">
                                            <select class="form-control " name="for_home" >
                                              <option value="Yes">Yes</option>
                                              <option value="No" selected>No</option>
                                              
                                            </select>
                                            
                                            <span class="help-block">This is required Select box</span>
                                        </div>
                                    </div>
-->
                                
                                <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">status</label>
                                        <div class="col-md-6 col-xs-12">
                                            <select class="form-control " name="status" >
                                              <option value="active">Active</option>
                                              <option value="inactivate">Inactivate</option>
                                              
                                            </select>
                                            
                                            <span class="help-block">This is required Select box</span>
                                        </div>
                                    </div>
                                     
                                      
                                    
                                          
			                    <div class="panel-footer">      
                                        <button type="button" class="btn btn-default" onclick="document.getElementById('cat_home').reset();">Clear Form</button>

                                        <button  type="submit" class="btn btn-primary pull-right" id="">Save <span class="fa fa-floppy-o fa-right"></span></button>
                             </div>
                        </form>
				   
                </div> 
           </div>
      </div>
              
<script>
$("#button_active").click(function(){
$("#postcheak_addhotel").prop('disabled', false);
});


</script>
    
