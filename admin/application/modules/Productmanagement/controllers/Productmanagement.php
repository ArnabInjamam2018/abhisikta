<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Productmanagement extends MY_Controller {
	
	function __construct() {		
		     parent::__construct();
			 $this->table_name="product_master";
			 $this->load->model("ProductManagement_Model");
			 
		}
		public function index()
	{
         $content['sub_cat_list']=$this->ProductManagement_Model->get_all_subcat();
         $content['cat_list']=$this->ProductManagement_Model->get_all_cat();
		 $content['products']=$this->ProductManagement_Model->fetch_all_products($this->table_name);
         $content['subview']="product_list";
		 $this->load->view('layout', $content);
	}
    
     public function catwisesubcat()
    {   
        $scat = $this->input->post('scat');

       $response=$this->ProductManagement_Model ->Get_subcat($scat);
      
       echo json_encode($response);
        
    }
    
    public function subcat_wise_subsub()
    {   
        $scat = $this->input->post('scat');

       $response=$this->ProductManagement_Model ->Get_sub_subcat($scat);
      
       echo json_encode($response);
        
    }
    
    public function cat()
    {   
        $scat = $this->input->post('scat');

       $response=$this->ProductManagement_Model ->Get_subcat($scat);
      
       echo json_encode($response);
        
    }
		
		function add_products(){
                    

                  
		        $content['sub_cat_list']=$this->ProductManagement_Model->get_all_subcat();
                $content['categories']=$this->ProductManagement_Model->get_all_categories();
		        $RequestMethod = $this->input->server('REQUEST_METHOD');
				if($RequestMethod == "POST")
			    {
					$product_images=$this->ProductManagement_Model->product_images_upload(); 
                    
                   $arr=explode(',',$this->input->post('cat_id'));
                   $cat_id=$arr[1];




					  $data=array(
                                    
                                    'product_name'=>$this->input->post('product_name'),
                                    'cat_id'=>$this->input->post('cat_id'),
                                    'pro_sub_cat_id'=>$this->input->post('subcat_id'),
                                    'product_description'=>$this->input->post('product_description'),
                                    'product_code'=>$this->input->post('product_code'),
                                    'status'=>$this->input->post('status'),
                                    'for_home'=>$this->input->post('for_home'),
                                    'pro_image'=>$product_images,
                                   
					             );
			   
		              $this->ProductManagement_Model->insert_data($this->table_name,$data);
                      $this->session->set_flashdata('alert', array('message' => 'Data successfully Saved','class' => 'success'));
	                  redirect('Productmanagement');	
				}
				else{
				   
                    $content['subview']="add_products";
                    $this->load->view('layout', $content);		
				}
				
		}


    function delet_products($getid)
	{
		 $id=decode_url($getid);
		 $get_img=$this->ProductManagement_Model->get_data($id,$this->table_name);
		
		 if($get_img){
			 $img_name=$get_img[0]->pro_image;
			 $id=$get_img[0]->pro_id;
			 $img_file=FCPATH . '/webroot/uploads/product/'.$img_name;
			 if(!unlink($img_file)) {} else { }
			 $this->ProductManagement_Model->delete_data($id,$this->table_name);
			 $this->session->set_flashdata('alert', array('message' => 'Deleted Product successfully','class' => 'success'));
			 redirect('Productmanagement');
			 
		 }else{
			 $this->session->set_flashdata('alert', array('message' => 'Deleted Product successfully','class' => 'error'));
			 redirect('Productmanagement');
			 
		 }
		 
	}

	function multi_pro_del()
	{
		$RequestMethod=$this->input->server('REQUEST_METHOD');
		if($RequestMethod== 'POST'){
			
			foreach($_POST['checklist'] as $id){
				$get_img=$this->ProductManagement_Model->get_data($id,$this->table_name);
				$img_name=$get_img[0]->pro_image;
			    $id=$get_img[0]->pro_id;
              
			    $img_file=FCPATH . '/webroot/uploads/product/'.$img_name;
			    if(!unlink($img_file)) {} else { }
			    $this->ProductManagement_Model->delete_data($id,$this->table_name);
				
			}
			$this->session->set_flashdata('alert', array('message' => 'Deleted Product successfully','class' => 'success'));
			 redirect('Productmanagement');
		}else{
			$this->session->set_flashdata('alert', array('message' => 'Not valid page','class' => 'error'));
			redirect('Productmanagement');
		}
	
		
	}


		
       
	function edit_product($getid)
	{
	  $id=decode_url($getid);
 
	  $content['product_edit']=$this->ProductManagement_Model->get_data($id,$this->table_name);
      $content['categories_asas']=$this->ProductManagement_Model->get_all_categories();
      $content['sub_cat_list']=$this->ProductManagement_Model->get_all_subcat();
      $content['sub_subcat_list']=$this->ProductManagement_Model->get_all_subsubcat();
      
	  $content['subview']="edit_product";
	  $this->load->view('layout', $content); 
		
	}
	function update_product()
	{
        
     
		$RequestMethod=$this->input->server("REQUEST_METHOD");
		if($RequestMethod == "POST"){
			
			
			$id=$this->input->post('product_id');
			if($_FILES['uploadedimages']['error'][0]>0){
				   $product_img_name=$this->input->post('prev_image_multipal');
				
				   } else {
						$product_detais=$this->ProductManagement_Model->get_data($id,$this->table_name);
                        printarray($product_detais);
						$previous_name=$product_detais[0]->pro_image;
						$p_id=$product_detais[0]->pro_id; 
						$img_file=FCPATH . '/webroot/uploads/product/'.$previous_name;
						if (!unlink($img_file)) {} else { }
						$product_img_name=$this->ProductManagement_Model->product_images_upload(); 
				       }


				   $cat_id=$this->input->post('cat_id');
				  

				 
                  

                

						$data=array(
						            'product_name'=>$this->input->post('product_name'),
                                    'cat_id'=>$this->input->post('cat_id'),
                                    'pro_sub_cat_id'=>$this->input->post('subcat_id'),
                                    'product_description'=>$this->input->post('product_description'),
                                    'product_code'=>$this->input->post('product_code'),
                                    'for_home'=>$this->input->post('for_home'),
                                    'pro_image'=>$product_img_name,
                                    'status'=>$this->input->post('status'),
                                   
                                   
                                   
			              );


						$this->ProductManagement_Model->update_data($id,$data,$this->table_name);	
							
						$this->session->set_flashdata('alert', array('message' => 'Update Product successfully','class' => 'success'));
			           redirect('Productmanagement');
						
						
					 
		}else{
			redirect('Productmanagement');
		}

	}

	function product_act_inactive($getid)
	{
		$id=$getid;
		$product_details=$this->ProductManagement_Model->get_data($id,$this->table_name);
		$status=$product_details[0]->status;
        
    
     
		if($status=='active')
		{
			$data=array('status'=>'inactivate');
           
			$this->ProductManagement_Model->update_data($id,$data,$this->table_name);	
							
		    $this->session->set_flashdata('alert', array('message' =>'Product status Update successfully','class' => 'success'));
			redirect('Productmanagement');
		}else{
			$data=array('status'=>'active');
			$this->ProductManagement_Model->update_data($id,$data,$this->table_name);	
							
		    $this->session->set_flashdata('alert', array('message' =>'Product status Update successfully','class' => 'success'));
			redirect('Productmanagement');
			
		}
	}


	// function product_act_inactive($getid)
	// {
	// 	$id=$getid;
	// 	$product_details=$this->ProductManagement_Model->get_data($id,$this->table_name);
	// 	$status=$product_details[0]->status;
        
    
     
	// 	if($status=='active')
	// 	{
	// 		$data=array('status'=>'inactivate');
           
	// 		$this->ProductManagement_Model->update_data($id,$data,$this->table_name);	
							
	// 	    $this->session->set_flashdata('alert', array('message' =>'Product status Update successfully','class' => 'success'));
	// 		redirect('Productmanagement');
	// 	}else{
	// 		$data=array('status'=>'active');
	// 		$this->ProductManagement_Model->update_data($id,$data,$this->table_name);	
							
	// 	    $this->session->set_flashdata('alert', array('message' =>'Product status Update successfully','class' => 'success'));
	// 		redirect('Productmanagement');
			
	// 	}
	// }


	
	function show_home_page_stat($getid)
	{
		$id=$getid;
		$product_details=$this->ProductManagement_Model->get_data($id,$this->table_name);
		$status=$product_details[0]->for_home;
        
    
     
		if($status=='Yes')
		{
			$data=array('for_home'=>'No');
           
			$this->ProductManagement_Model->update_data($id,$data,$this->table_name);	
							
		    $this->session->set_flashdata('alert', array('message' =>'Product status Update successfully','class' => 'success'));
			redirect('Productmanagement');
		}else{
			$data=array('for_home'=>'Yes');
			$this->ProductManagement_Model->update_data($id,$data,$this->table_name);	
							
		    $this->session->set_flashdata('alert', array('message' =>'Product status Update successfully','class' => 'success'));
			redirect('Productmanagement');
			
		}
	}
    
        
       
		
		
	  
	
	
}	