   
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation" id="navigation">
                    <li class="xn-logo">
                        <a href="<?php echo site_url('dashboard');?>" id="logoclass">
						<?php if(!empty($this->website['loginuser']->profile_pic)) { ?>
						<?php echo $this->website['data']->company_name;?></a>
						<?php }else{ ?>
						Dashboard
						<?php } ?>
                        <a href="javascript:void(0);" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="javascript:void(0);" class="profile-mini">
							<?php if(!empty($this->website['loginuser']->profile_pic)) { ?>
                               <img src="<?php echo site_url().'/webroot/uploads/profile-pic/'.$this->website['loginuser']->profile_pic;?>" class="img-thumbnail" alt="User Image" />
							<?php } else { ?> 
                                <img src="<?php echo site_url('webroot/uploads/profile-pic/no-image.png');?>" class="img-thumbnail" alt="User Image" />

							<?php } ?>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
							<?php if(!empty($this->website['loginuser']->profile_pic)) { ?>
                               <img src="<?php echo site_url().'/webroot/uploads/profile-pic/'.$this->website['loginuser']->profile_pic;?>" class="img-thumbnail" alt="User Image" />
							<?php } else { ?> 
                                <img src="<?php echo site_url('webroot/uploads/profile-pic/no-image.png');?>" class="img-thumbnail" alt="User Image" />

							<?php } ?>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?php echo $this->website['loginuser']->first_name.' '.$this->website['loginuser']->last_name;?></div>
                                <div class="profile-data-title">Last Login : <?php echo $this->session->userdata('loginDetail')->last_login;?></div>
                            </div>
                       
                        </div>                                                                        
                    </li>
                   <?php if($this->session->userdata('loginDetail')->role == "Super Admin"){ ?>
                    <li class="not-openable">
                        <a href="<?php echo site_url('dashboard'); ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li>  
                    


                    
                    
                    <li class="xn-openable">
						<a href="JavaScript:void(0);"><span class="fa fa-picture-o"></span> <span class="xn-text">Banner Management</span></a>
							 <ul class="ul-open">
							 <li><a href="<?php echo site_url('banner');?>"><span class="fa fa-list"></span>Banner list </a></li>
							  <li><a href="<?php echo site_url('banner/add_banner');?>"><span class="fa fa-plus-circle"></span> Add Banner </a></li>
							 </ul>
                     </li>
                    
                    <!-- <li class="xn-openable">
						<a href="JavaScript:void(0);"><span class="fa fa-picture-o"></span> <span class="xn-text">Brand Management</span></a>
							 <ul class="ul-open">
							 <li><a href="<?php echo site_url('gallery');?>"><span class="fa fa-list"></span>Brand list </a></li>
							  <li><a href="<?php echo site_url('gallery/add_gallery');?>"><span class="fa fa-plus-circle"></span> Add Brand Images </a></li>
							 </ul>
                     </li> -->
                            
                    <li><a href="<?php echo site_url('partner/').base64_encode('list').'/'.EncryptID(1);?>"><span class="fa fa-list"></span> Clients  Management</a>
                    </li>
                   

                    
		
					
					
				

                     <li class="xn-openable">
                        <a href="JavaScript:void(0);"><span class="fa fa-user-plus "></span> <span class="xn-text">Location Management</span></a>
                      <ul class="ul-open"> 
                            
                                                                                                               

                                     
                                                   
                           <li><a href="<?php echo site_url('Categories');?>"><span class="fa fa-list"></span>  Location Category List</a></li>
                           <li><a href="<?php echo site_url('Categories/add_categories');?>"><span class="fa fa-plus-circle"></span>Add Location</a></li>
                                     
                             
                                                                               
                            <!-- <li><a href="<?php echo site_url('Subcategories');?>"><span class="fa fa-list"></span>Manage SubCategory List</a></li>
                            <li><a href="<?php echo site_url('Subcategories/add_subcategories');?>"><span class="fa fa-plus-circle"></span>Add Sub Category</a></li> -->
                                  
                      </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="JavaScript:void(0);"><span class="fa fa-files-o"></span> <span class="xn-text">Project Management </span></a>
                        <ul class="ul-open">
                            <li><a href="<?php echo site_url('Productmanagement/add_products');?>"><span class="fa fa-plus-circle"></span> Add  Project </a></li>						
                            <li><a href="<?php echo site_url('Productmanagement');?>"><span class="fa fa-list-alt"></span>Added Project List</a></li>      
                        </ul>
                    </li>
                    
                    <li class="xn-openable">
                        <a href="JavaScript:void(0);"><span class="fa fa-files-o"></span> <span class="xn-text">Service Management </span></a>
                        <ul class="ul-open">
                            <li><a href="<?php echo site_url('Servicemanagement/add_services');?>"><span class="fa fa-plus-circle"></span> Add  Service </a></li>						
                            <li><a href="<?php echo site_url('Servicemanagement');?>"><span class="fa fa-list-alt"></span>Added Service List</a></li>      
                        </ul>
                    </li>

                    <li class="xn-openable">
                            <a href="JavaScript:void(0);"><span class="fa fa-picture-o"></span> <span class="xn-text"> Our Team</span></a>
                                 <ul class="ul-open">
                                  <li><a href="<?php echo site_url('Manage_user');?>"><span class="fa fa-list"></span>Team list </a></li>  
                                  <li><a href="<?php echo site_url('Manage_user/add_input');?>"><span class="fa fa-plus-circle"></span> Add Team </a></li>
                                 </ul>
                        </li>

                        <li class="xn-openable">
                            <a href="JavaScript:void(0);"><span class="fa fa-picture-o"></span> <span class="xn-text">Testimonial Management</span></a>
                                 <ul class="ul-open">
                                <li><a href="<?php echo site_url('Testimonial');?>"><span class="fa fa-list"></span>Testimonial list </a></li>


                                 <li><a href="<?php echo site_url('Testimonial/add_testimonial');?>"><span class="fa fa-plus-circle"></span> Add Testimonial </a></li>
                                   
                                 </ul>
                         </li>

                         <li class="xn-openable">
                        <a href="JavaScript:void(0);"><span class="fa fa-picture-o"></span> <span class="xn-text">Content Management</span></a>
                             <ul class="ul-open">
                             <li><a href="<?php echo base_url('Homepagecontent'); ?>"><span class="fa fa-list"></span> Master </a></li>       
                             </ul>
                     </li>

                    
             
                         
                   
					<li class="xn-openable">
                        <a href="JavaScript:void(0);"><span class="fa fa-question-circle"></span> <span class="xn-text">Enquiry Management </span></a>
                        <ul class="ul-open">
                            <li><a href="<?php echo site_url('query');?>"><span class="fa fa-plus-circle"></span> Portal Enquiry </a></li>

                        </ul>
                    </li>
					
					
					
				<?php 
				 } else{
					$permit=unserialize($this->session->userdata('loginDetail')->permissions);
					
				?>
				<li class="not-openable">
                        <a href="<?php echo site_url('dashboard'); ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                </li>
					 <?php if(in_array("Banner",$permit)){ ?>
				  <li class="xn-openable">
					 <a href="JavaScript:void(0);"><span class="fa fa-picture-o "></span> <span class="xn-text">Banner Management</span></a>
					 <ul class="ul-open">
					 <li><a href="<?php echo site_url('banner');?>"><span class="fa fa-plus-circle"></span>Banner list </a></li>
					  <li><a href="<?php echo site_url('Add-Banner');?>"><span class="fa fa-plus-circle"></span> Add Banner </a></li>
					 
					 </ul>
              </li>
				 <?php } ?>
				 
			
				   
				   
				 
				  

				 
				   <?php } ?>
	
                   
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->